-- Table: public.administrator

-- DROP TABLE public.administrator;

CREATE TABLE public.administrator
(
    id integer NOT NULL,
    email character varying(255) COLLATE pg_catalog."default",
    level integer,
    password character varying(255) COLLATE pg_catalog."default",
    username character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT administrator_pkey PRIMARY KEY (id),
    CONSTRAINT administrator_username_key UNIQUE (username)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.administrator
    OWNER to ear2019zs_22;



-- Table: public.bus

-- DROP TABLE public.bus;

CREATE TABLE public.bus
(
    id integer NOT NULL,
    description character varying(255) COLLATE pg_catalog."default",
    drivenkilometers integer,
    luggagespace integer,
    seats integer,
    yearofmanufacture integer,
    CONSTRAINT bus_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.bus
    OWNER to ear2019zs_22;



-- Table: public.busline

-- DROP TABLE public.busline;

CREATE TABLE public.busline
(
    id integer NOT NULL,
    datetime timestamp without time zone,
    regularity integer,
    driver_id integer,
    bus_id integer,
    CONSTRAINT busline_pkey PRIMARY KEY (id),
    CONSTRAINT fk_busline_bus_id FOREIGN KEY (bus_id)
        REFERENCES public.bus (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_busline_driver_id FOREIGN KEY (driver_id)
        REFERENCES public.driver (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.busline
    OWNER to ear2019zs_22;



-- Table: public.busline_destinationkilometerminutes

-- DROP TABLE public.busline_destinationkilometerminutes;

CREATE TABLE public.busline_destinationkilometerminutes
(
    busline_id integer NOT NULL,
    id integer NOT NULL,
    destination_id integer NOT NULL,
    CONSTRAINT busline_destinationkilometerminutes_pkey PRIMARY KEY (busline_id, id, destination_id),
    CONSTRAINT fk_busline_destinationkilometerminutes_busline_id FOREIGN KEY (busline_id)
        REFERENCES public.busline (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_busline_destinationkilometerminutes_id FOREIGN KEY (id, busline_id, destination_id)
        REFERENCES public.destinationkilometerminutes (id, busline_id, destination_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.busline_destinationkilometerminutes
    OWNER to ear2019zs_22;



-- Table: public.customer

-- DROP TABLE public.customer;

CREATE TABLE public.customer
(
    id integer NOT NULL,
    email character varying(255) COLLATE pg_catalog."default",
    name character varying(255) COLLATE pg_catalog."default",
    password character varying(255) COLLATE pg_catalog."default",
    username character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT customer_pkey PRIMARY KEY (id),
    CONSTRAINT customer_username_key UNIQUE (username)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.customer
    OWNER to ear2019zs_22;



-- Table: public.customer_ticket

-- DROP TABLE public.customer_ticket;

CREATE TABLE public.customer_ticket
(
    customer_id integer NOT NULL,
    tickets_id bigint NOT NULL,
    CONSTRAINT customer_ticket_pkey PRIMARY KEY (customer_id, tickets_id),
    CONSTRAINT fk_customer_ticket_customer_id FOREIGN KEY (customer_id)
        REFERENCES public.customer (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_customer_ticket_tickets_id FOREIGN KEY (tickets_id)
        REFERENCES public.ticket (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.customer_ticket
    OWNER to ear2019zs_22;



-- Table: public.destination

-- DROP TABLE public.destination;

CREATE TABLE public.destination
(
    id integer NOT NULL,
    country integer,
    name character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT destination_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.destination
    OWNER to ear2019zs_22;



-- Table: public.destination_destination

-- DROP TABLE public.destination_destination;

CREATE TABLE public.destination_destination
(
    destination_id integer NOT NULL,
    connecteddestinations_id integer NOT NULL,
    CONSTRAINT destination_destination_pkey PRIMARY KEY (destination_id, connecteddestinations_id),
    CONSTRAINT fk_destination_destination_connecteddestinations_id FOREIGN KEY (connecteddestinations_id)
        REFERENCES public.destination (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_destination_destination_destination_id FOREIGN KEY (destination_id)
        REFERENCES public.destination (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.destination_destination
    OWNER to ear2019zs_22;



-- Table: public.destinationkilometerminutes

-- DROP TABLE public.destinationkilometerminutes;

CREATE TABLE public.destinationkilometerminutes
(
    id integer NOT NULL,
    kilometersfromprevious integer,
    minutesfromprevious integer,
    busline_id integer NOT NULL,
    destination_id integer NOT NULL,
    CONSTRAINT destinationkilometerminutes_pkey PRIMARY KEY (id, busline_id, destination_id),
    CONSTRAINT fk_destinationkilometerminutes_busline_id FOREIGN KEY (busline_id)
        REFERENCES public.busline (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_destinationkilometerminutes_destination_id FOREIGN KEY (destination_id)
        REFERENCES public.destination (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.destinationkilometerminutes
    OWNER to ear2019zs_22;



-- Table: public.driver

-- DROP TABLE public.driver;

CREATE TABLE public.driver
(
    id integer NOT NULL,
    age integer,
    email character varying(255) COLLATE pg_catalog."default",
    minutesdriveninrow integer,
    password character varying(255) COLLATE pg_catalog."default",
    username character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT driver_pkey PRIMARY KEY (id),
    CONSTRAINT driver_username_key UNIQUE (username)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.driver
    OWNER to ear2019zs_22;



-- Table: public.driver_busline

-- DROP TABLE public.driver_busline;

CREATE TABLE public.driver_busline
(
    driver_id integer NOT NULL,
    lines_id integer NOT NULL,
    CONSTRAINT driver_busline_pkey PRIMARY KEY (driver_id, lines_id),
    CONSTRAINT fk_driver_busline_driver_id FOREIGN KEY (driver_id)
        REFERENCES public.driver (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_driver_busline_lines_id FOREIGN KEY (lines_id)
        REFERENCES public.busline (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.driver_busline
    OWNER to ear2019zs_22;



-- Table: public.ticket

-- DROP TABLE public.ticket;

CREATE TABLE public.ticket
(
    id bigint NOT NULL,
    arrivaltime timestamp without time zone,
    datetime timestamp without time zone,
    departuretime timestamp without time zone,
    length integer,
    busline_id integer,
    customer_id integer,
    finaldestination_id integer,
    startingdestination_id integer,
    CONSTRAINT ticket_pkey PRIMARY KEY (id),
    CONSTRAINT fk_ticket_busline_id FOREIGN KEY (busline_id)
        REFERENCES public.busline (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_ticket_customer_id FOREIGN KEY (customer_id)
        REFERENCES public.customer (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_ticket_finaldestination_id FOREIGN KEY (finaldestination_id)
        REFERENCES public.destination (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_ticket_startingdestination_id FOREIGN KEY (startingdestination_id)
        REFERENCES public.destination (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.ticket
    OWNER to ear2019zs_22;




