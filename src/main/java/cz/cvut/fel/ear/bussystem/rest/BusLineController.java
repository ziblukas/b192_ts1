package cz.cvut.fel.ear.bussystem.rest;

import cz.cvut.fel.ear.bussystem.exceptions.ForbiddenException;
import cz.cvut.fel.ear.bussystem.exceptions.NotFoundException;
import cz.cvut.fel.ear.bussystem.model.BusLine;
import cz.cvut.fel.ear.bussystem.model.Driver;
import cz.cvut.fel.ear.bussystem.rest.util.RestUtils;
import cz.cvut.fel.ear.bussystem.security.RoleChecker;
import cz.cvut.fel.ear.bussystem.service.BusLineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/rest/buslines")
public class BusLineController {

    private static final Logger LOG = LoggerFactory.getLogger(BusLineController.class);

    @Autowired
    private BusLineService busLineService;

    @Autowired
    private RoleChecker roleChecker;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public BusLine getBusLine(@PathVariable Integer id) {
        BusLine busLine = busLineService.find(id);
        if (busLine == null)
            throw NotFoundException.create("BusLine", id);
        return busLine;
    }

    @PostMapping(value = "/busline", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> creteBusLine(@RequestBody BusLine busLine) {
        if (roleChecker.isAdmin()) {
            busLineService.addBusLine(busLine);
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", busLine.getId());
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
    }

    @PutMapping(value = "/busline/driver", consumes = MediaType.APPLICATION_JSON_VALUE)
    public BusLine updateDriverOfBusLine(@RequestBody BusLine busLine, @RequestBody Driver driver) {
        if (roleChecker.isAdmin())
            return busLineService.updateDriverOfBusLine(busLine, driver);
        return busLineService.find(busLine.getId());
    }

    @PutMapping(value = "/busline/date", consumes = MediaType.APPLICATION_JSON_VALUE)
    public BusLine updateBusLineDateTime(@RequestBody BusLine busLine, @RequestBody Date date) {
        if (roleChecker.isAdmin())
            return busLineService.updateBusLineDateTime(busLine, date);
        return busLineService.find(busLine.getId());
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/bus", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void removeBusLine(@RequestBody BusLine busLine) {
        if (busLineService.find(busLine.getId()) == null) //TODO why null?
            throw NotFoundException.create("Busline", busLine.getId());
        if (roleChecker.isAdmin())
            busLineService.removeBusLine(busLine);
        else
            throw ForbiddenException.create();
    }

}
