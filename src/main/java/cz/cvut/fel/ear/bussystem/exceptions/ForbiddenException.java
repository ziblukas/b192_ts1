package cz.cvut.fel.ear.bussystem.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN)
public class ForbiddenException extends EarException {
    public static ForbiddenException create() {
        return new ForbiddenException();
    }
}
