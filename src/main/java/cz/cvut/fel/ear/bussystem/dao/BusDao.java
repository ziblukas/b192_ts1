package cz.cvut.fel.ear.bussystem.dao;

import cz.cvut.fel.ear.bussystem.model.Bus;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class BusDao extends BaseDao<Bus> {
    
    public BusDao() {
        super(Bus.class);
    }

    @Override
    public List<Bus> findAll() {
                        // query: "SELECT b FROM Bus b WHERE NOT b.removed"
        return em.createQuery("SELECT b FROM Bus b", Bus.class).getResultList();
    }

    public Bus find(Integer id) {
        if (id != null)
            return em.find(Bus.class, id);
        else
            return null;
    }
}
