package cz.cvut.fel.ear.bussystem.model;

public enum Luggage {
    NONE,
    HAND_LUGGAGE,
    LESS_THAN_8M_SQUARE,
    MORE_THAN_8M_SQUARE,
}
