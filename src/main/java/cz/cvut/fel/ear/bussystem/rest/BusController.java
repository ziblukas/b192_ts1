package cz.cvut.fel.ear.bussystem.rest;

import cz.cvut.fel.ear.bussystem.exceptions.ForbiddenException;
import cz.cvut.fel.ear.bussystem.exceptions.NotFoundException;
import cz.cvut.fel.ear.bussystem.model.Bus;
import cz.cvut.fel.ear.bussystem.rest.util.RestUtils;
import cz.cvut.fel.ear.bussystem.security.RoleChecker;
import cz.cvut.fel.ear.bussystem.service.BusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/busses")
public class BusController {

    private static final Logger LOG = LoggerFactory.getLogger(BusController.class);

    @Autowired
    private BusService busService;

    //Because tests are not using my bean and i dont know why
    private RoleChecker roleChecker = new RoleChecker();

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Bus getBus(@PathVariable Integer id) {
        final Bus bus = busService.find(id);
        if (bus == null)
            throw NotFoundException.create("Bus", id);
        return bus;
    }

    @PostMapping(value = "/bus", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createBus(@RequestBody Bus bus) {
        if (roleChecker.isAdmin()) {
            busService.addBus(bus);
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", bus.getId());
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
    }

    @PutMapping(value = "/bus", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Bus updateBus(@RequestBody Bus bus) {
        if (roleChecker.isAdmin())
            return busService.updateBus(bus);
        throw ForbiddenException.create();
    }

    @PutMapping(value = "/bus/{kilometers}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateKilometers(@RequestBody Bus bus, @PathVariable Integer kilometers) {
        if (roleChecker.isDriver() || roleChecker.isAdmin())
            busService.addDrivenKilometers(bus, kilometers);
        else
            throw ForbiddenException.create();
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/bus", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void removeBus(@RequestBody Bus bus) {
        if (busService.find(bus.getId()) == null)
            throw NotFoundException.create("Destination", bus.getId());
        if (roleChecker.isAdmin())
            busService.removeBus(bus);
        else
            throw ForbiddenException.create();
    }
}
