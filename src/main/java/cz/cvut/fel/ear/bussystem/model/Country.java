package cz.cvut.fel.ear.bussystem.model;

public enum Country {
    CZECH,
    GERMANY,
    POLAND,
    SLOVAK,
    HUNGARY,
    AUSTRIA,
}
