package cz.cvut.fel.ear.bussystem.rest;

import cz.cvut.fel.ear.bussystem.model.Administrator;
import cz.cvut.fel.ear.bussystem.model.User;
import cz.cvut.fel.ear.bussystem.rest.util.RestUtils;
import cz.cvut.fel.ear.bussystem.security.DefaultAuthenticationProvider;
import cz.cvut.fel.ear.bussystem.security.RoleChecker;
import cz.cvut.fel.ear.bussystem.security.SecurityUtils;
import cz.cvut.fel.ear.bussystem.service.AdministratorService;
import cz.cvut.fel.ear.bussystem.service.CustomerService;
import cz.cvut.fel.ear.bussystem.service.DriverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest")
public class LoginController {

    @Autowired
    private DefaultAuthenticationProvider provider;

    @Autowired
    private RoleChecker roleChecker;

    private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);

    private final CustomerService customerService;
    private final DriverService driverService;
    private final AdministratorService administratorService;

    @Autowired
    public LoginController(CustomerService customerService, DriverService driverService, AdministratorService administratorService) {
        this.customerService = customerService;
        this.driverService = driverService;
        this.administratorService = administratorService;
    }

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> login(@RequestBody Administrator user) {
        boolean logged = false;
        User userToLogIn = driverService.validatePassword(user.getUsername(), user.getPassword());
        if (userToLogIn == null)
            userToLogIn = administratorService.validatePassword(user.getUsername(), user.getPassword());
        if (userToLogIn == null)
            userToLogIn = customerService.validatePassword(user.getUsername(), user.getPassword());
        if (userToLogIn != null) {
            final Authentication auth = new UsernamePasswordAuthenticationToken(userToLogIn.getUsername(), userToLogIn.getPassword());
            final Authentication result = provider.authenticate(auth);
            logged = true;
        }

        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/current");
        if (logged) {
            LOG.debug("User {} successfully logged.", user);
            return new ResponseEntity<>(headers, HttpStatus.OK);
        } else {
            LOG.debug("User {} can not be logged.", user);
            return new ResponseEntity<>(headers, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(value = "/logged", produces = MediaType.APPLICATION_JSON_VALUE)
    public User getCurrent() {
        if (roleChecker.isLogged())
            return SecurityUtils.getCurrentUser();
        return null;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_CUSTOMER', 'ROLE_DRIVER')")
    @PostMapping(value = "/logout")
    public ResponseEntity<Void> logOut() {
        SecurityUtils.setCurrentUser(null);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/current");
        return new ResponseEntity<>(headers, HttpStatus.OK);
    }

}
