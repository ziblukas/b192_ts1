package cz.cvut.fel.ear.bussystem.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Getter
@Setter
public class Ticket implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Customer customer;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private BusLine busLine;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Destination startingDestination;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Destination finalDestination;
    private int length;
    private Date dateTime;
    private Date departureTime;
    private Date arrivalTime;
}
