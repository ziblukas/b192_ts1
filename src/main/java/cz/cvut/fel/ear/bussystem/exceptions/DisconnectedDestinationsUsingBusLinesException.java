/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.ear.bussystem.exceptions;

/**
 *
 * @author kushallus
 */
public class DisconnectedDestinationsUsingBusLinesException extends EarException {

    public DisconnectedDestinationsUsingBusLinesException(String message) {
        super(message);
    }
}
