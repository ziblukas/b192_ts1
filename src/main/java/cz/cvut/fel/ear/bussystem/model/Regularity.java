package cz.cvut.fel.ear.bussystem.model;

public enum Regularity {
    DAILY,
    WEEKLY,
    MONTHLY,
    WORKDAY,
}
