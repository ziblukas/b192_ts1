package cz.cvut.fel.ear.bussystem.service;

import cz.cvut.fel.ear.bussystem.dao.DestinationDao;
import cz.cvut.fel.ear.bussystem.model.Destination;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DestinationService {
    private final DestinationDao dao;
    
    @Autowired
    public DestinationService(DestinationDao dao) {
        this.dao = dao;
    }

    @Transactional
    public Destination find(Integer id) {
        return dao.find(id);
    }
    
    @Transactional
    public Destination addDestination(Destination destination) {
        if (destination != null)
            dao.persist(destination);
        return destination;
    }
    
    @Transactional
    public void connectTwoDestinations(Destination dest1, Destination dest2) {
        if (dest1 != null && dest2 != null) {
            List<Destination> destinations = dest1.getConnectedDestinations();
            destinations.add(dest2);
            dest1.setConnectedDestinations(destinations);
            destinations = dest2.getConnectedDestinations();
            destinations.add(dest1);
            dest2.setConnectedDestinations(destinations);
            dao.update(dest1);
            dao.update(dest2);
        }
    }
    
    @Transactional
    public void removeDestination(Destination destination) {
        if (destination != null)
            dao.remove(destination);
    }
}
