package cz.cvut.fel.ear.bussystem.model;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Destination extends AbstractEntity {
    @ManyToMany(cascade = CascadeType.PERSIST)
    @OrderBy("name")
    private List<Destination> connectedDestinations;
    @Enumerated
    private Country country;
    private String name;
}
