package cz.cvut.fel.ear.bussystem.dao;

import cz.cvut.fel.ear.bussystem.model.Administrator;
import cz.cvut.fel.ear.bussystem.model.Customer;
import cz.cvut.fel.ear.bussystem.model.Driver;
import cz.cvut.fel.ear.bussystem.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class UserDao extends BaseDao<User> {

    public UserDao() {
        super(User.class);
    }

    public User findByUsername(String username) {
        if (username != null) {
            User ret = null;
            try {
                ret = em.createNamedQuery("Administrator.findByUsername", Administrator.class).setParameter("username", username).getSingleResult();
                if (ret != null)
                    return ret;
            } catch (NoResultException e) {}
            try {
                ret = em.createNamedQuery("Customer.findByUsername", Customer.class).setParameter("username", username).getSingleResult();
                if (ret != null)
                    return ret;
            } catch (NoResultException e) {}
            try {
                ret = em.createNamedQuery("Driver.findByUsername", Driver.class).setParameter("username", username).getSingleResult();
                if (ret != null)
                    return ret;
            } catch (NoResultException e) {
                System.out.println("Non-existing username: " + username + "!");
            }
            return null;
        } else
            return null;
        
    }

    public boolean exists(String username) {
        if (username != null)
            return findByUsername(username) != null;
        else
            return false;
    }

}
