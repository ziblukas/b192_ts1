package cz.cvut.fel.ear.bussystem.rest;

import cz.cvut.fel.ear.bussystem.exceptions.ForbiddenException;
import cz.cvut.fel.ear.bussystem.exceptions.NotFoundException;
import cz.cvut.fel.ear.bussystem.model.Destination;
import cz.cvut.fel.ear.bussystem.rest.util.RestUtils;
import cz.cvut.fel.ear.bussystem.security.RoleChecker;
import cz.cvut.fel.ear.bussystem.service.DestinationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/destinations")
public class DestinationController {

    private static final Logger LOG = LoggerFactory.getLogger(DestinationController.class);

    @Autowired
    private DestinationService destinationService;

    @Autowired
    private RoleChecker roleChecker;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Destination getDestination(@PathVariable Integer id) {
        final Destination destination = destinationService.find(id);
        if (destination == null)
            throw NotFoundException.create("Destination", id);
        return destination;
    }

    @PostMapping(value = "/destination", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> creteDestination(@RequestBody Destination destination) {
        if (roleChecker.isAdmin()) {
            destinationService.addDestination(destination);
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", destination.getId());
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
    }

    @PutMapping(value = "/connect", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void connectTwoDestinations(@RequestBody Destination destination1, @RequestBody Destination destination2) {
        if (roleChecker.isAdmin())
            destinationService.connectTwoDestinations(destination1, destination2);
        else
            throw ForbiddenException.create();
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/destination", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void removeDestination(@RequestBody Destination destination) {
        if (destinationService.find(destination.getId()) == null)
            throw NotFoundException.create("Destination", destination.getId());
        if (roleChecker.isAdmin())
            destinationService.removeDestination(destination);
        else
            throw ForbiddenException.create();
    }

}
