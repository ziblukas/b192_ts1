package cz.cvut.fel.ear.bussystem.exceptions;

/**
 * Indicates that an error occurred when trying to access or work with a user's cart.
 */
public class BusAccessException extends EarException {

    public BusAccessException(String message) {
        super(message);
    }
}
