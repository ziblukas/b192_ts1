package cz.cvut.fel.ear.bussystem.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "Driver.findByUsername", query = "SELECT u FROM Driver u WHERE u.username = :username")
})
public class Driver extends User {
    private int age;
    private int minutesDrivenInRow;
    @OneToMany(cascade = CascadeType.PERSIST)
    @OrderBy("dateTime")
    private List<BusLine> lines;

    public Driver(String username, String password, String email) {
        super(username, password, email);
    }

    public Driver() {
        super("","","");
    }

    @Override
    public String getRole() {
        return "ROLE_DRIVER";
    }

    @Override
    public void erasePassword() {
        //TODO
    }
}
