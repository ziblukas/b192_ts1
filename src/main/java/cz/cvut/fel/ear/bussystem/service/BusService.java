package cz.cvut.fel.ear.bussystem.service;

import cz.cvut.fel.ear.bussystem.dao.BusDao;
import cz.cvut.fel.ear.bussystem.model.Bus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BusService {
    private final BusDao dao;
    
    @Autowired
    public BusService(BusDao dao) {
        this.dao = dao;
    }

    @Transactional
    public Bus find(Integer id) {
        return dao.find(id);
    }
    
    @Transactional
    public Bus addBus(Bus bus) {
        if (bus != null)
            dao.persist(bus);
        return bus;
    }
    
    @Transactional
    public Bus updateBus(Bus bus) {
        if (bus != null)
            dao.update(bus);
        return bus;
    }
    
    @Transactional
    public void addDrivenKilometers(Bus bus, int kilometers) {
        if (bus != null && kilometers > 0) {
            bus.setDrivenKilometers(bus.getDrivenKilometers() + kilometers);
            dao.update(bus);
        }
    }
    
    @Transactional
    public void removeBus(Bus bus) {
        if (bus != null)
            dao.remove(bus);
    }
}
