package cz.cvut.fel.ear.bussystem.service;

import cz.cvut.fel.ear.bussystem.dao.CustomerDao;
import cz.cvut.fel.ear.bussystem.dao.UserDao;
import cz.cvut.fel.ear.bussystem.model.Customer;
import cz.cvut.fel.ear.bussystem.model.Ticket;
import cz.cvut.fel.ear.bussystem.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CustomerService {
    private final CustomerDao customerDao;
    private final UserDao userDao;
    
    @Autowired
    public CustomerService(CustomerDao customerDao, UserDao userDao) {
        this.customerDao = customerDao;
        this.userDao = userDao;
    }

    @Transactional
    public Customer find(Integer id) {
        return customerDao.find(id);
    }
    
    @Transactional
    public Customer addCustomer(Customer customer) {
        if (customer != null) {
            User user = userDao.findByUsername(customer.getUsername());
            if (user == null)
                return null;
            customerDao.persist(customer);
        }
        return customer;
    }
    
    @Transactional
    public void addTicketToCustomer(Customer customer, Ticket ticket) {
        if (customer != null && ticket != null) {
            List<Ticket> tickets = customer.getTickets();
            tickets.add(ticket);
            customer.setTickets(tickets);
            customerDao.update(customer);
        }
    }
    
    @Transactional
    public Customer updateEmail(Customer customer, String email) {
        if (customer != null && email != null) {
            customer.setEmail(email);
            customerDao.update(customer);
            return customer;
        } else
            return null;
    }
    
    @Transactional
    public void removeCustomer(Customer customer) {
        if (customer != null)
            customerDao.remove(customer);
    }

    @Transactional
    public boolean exists(String username) {
        return userDao.exists(username);
    }
    
    @Transactional
    public User validatePassword(String username, String password) {
        User user = userDao.findByUsername(username);
        if (user != null && user.getPassword().equals(password))
            return user;
        else
            return null;
    }
}
