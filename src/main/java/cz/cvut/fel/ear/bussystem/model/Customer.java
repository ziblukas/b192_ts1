package cz.cvut.fel.ear.bussystem.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "Customer.findByUsername", query = "SELECT u FROM Customer u WHERE u.username = :username")
})
public class Customer extends User {
    private String name;
    @OneToMany(cascade = CascadeType.PERSIST)
    @OrderBy("dateTime")
    private List<Ticket> tickets;

    public Customer(String username, String password, String email) {
        super(username, password, email);
    }

    public Customer() {
        super("","","");
    }

    @Override
    public String getRole() {
        return "ROLE_CUSTOMER";
    }

    @Override
    public void erasePassword() {
        //TODO
    }
}
