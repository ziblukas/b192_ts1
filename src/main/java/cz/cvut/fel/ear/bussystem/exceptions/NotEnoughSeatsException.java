/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.ear.bussystem.exceptions;

import cz.cvut.fel.ear.bussystem.model.Ticket;

/**
 *
 * @author kushallus
 */
public class NotEnoughSeatsException extends EarException {

    public NotEnoughSeatsException(String message) {
        super(message);
    }

    public NotEnoughSeatsException(String message, Throwable cause) {
        super(message, cause);
    }

    public static NotEnoughSeatsException create(Ticket ticket) {
        return new NotEnoughSeatsException("Ticket on Bus Line from " + ticket.getStartingDestination().getName()
                + " to " + ticket.getFinalDestination().getName() + " on " + ticket.getArrivalTime().toString()
                + " cannot be created bacause of already taken seats in Bus.");
    }
}