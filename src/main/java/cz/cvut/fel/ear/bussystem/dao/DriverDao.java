package cz.cvut.fel.ear.bussystem.dao;

import cz.cvut.fel.ear.bussystem.model.Driver;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;

@Repository
public class DriverDao extends BaseDao<Driver> {
    
    public DriverDao() {
        super(Driver.class);
    }
    
    public Driver findByUsername(String username) {
        if (username != null) {
            try {
                return em.createNamedQuery("Driver.findByUsername", Driver.class).setParameter("username", username)
                         .getSingleResult();
            } catch (NoResultException e) {
                return null;
            }
        } else
            return null;
    }
}
