package cz.cvut.fel.ear.bussystem.exceptions;

/**
 * Indicates that insufficient amount of a product is available for processing, e.g. for creating order items.
 */
public class InsufficientDestinationException extends EarException {

    public InsufficientDestinationException(String message) {
        super(message);
    }
}
