package cz.cvut.fel.ear.bussystem.dao;

import cz.cvut.fel.ear.bussystem.model.Administrator;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;

@Repository
public class AdministratorDao extends BaseDao<Administrator> {
    
    public AdministratorDao() {
        super(Administrator.class);
    }
    
    public Administrator findByUsername(String username) {
        if (username != null) {
            try {
                return em.createNamedQuery("Administrator.findByUsername", Administrator.class).setParameter("username", username)
                         .getSingleResult();
            } catch (NoResultException e) {
                return null;
            }
        } else
            return null;
    }
}
