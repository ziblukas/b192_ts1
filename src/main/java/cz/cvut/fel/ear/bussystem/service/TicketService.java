package cz.cvut.fel.ear.bussystem.service;

import cz.cvut.fel.ear.bussystem.dao.BusLineDao;
import cz.cvut.fel.ear.bussystem.dao.TicketDao;
import cz.cvut.fel.ear.bussystem.exceptions.DisconnectedDestinationsUsingBusLinesException;
import cz.cvut.fel.ear.bussystem.exceptions.InsufficientDestinationException;
import cz.cvut.fel.ear.bussystem.exceptions.NotEnoughSeatsException;
import cz.cvut.fel.ear.bussystem.model.BusLine;
import cz.cvut.fel.ear.bussystem.model.Destination;
import cz.cvut.fel.ear.bussystem.model.DestinationKilometerMinutes;
import cz.cvut.fel.ear.bussystem.model.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import org.apache.commons.lang.time.DateUtils;

@Service
public class TicketService {
    private final TicketDao dao;
    private final BusLineDao busLineDao;
    
    @Autowired
    public TicketService(TicketDao dao, BusLineDao busLineDao) {
        this.dao = dao;
        this.busLineDao = busLineDao;
    }

    @Transactional
    public Ticket find(Long id) {
        if (id == null)
            return null;
        else
            return dao.find(id);
    }
    
    @Transactional
    public Ticket addTicket(Ticket ticket) {
        if (ticket != null) {
            long lineTickets = dao.findAll().stream()
                    .filter(t -> t.getBusLine() == ticket.getBusLine())
                    .count();
            if (ticket.getBusLine().getBus().getSeats() > lineTickets) {
                for (DestinationKilometerMinutes dkmm : ticket.getBusLine().getDestinations()) {
                    dkmm.setBusLine(ticket.getBusLine());
                }
                dao.persist(ticket);
            } else
                throw NotEnoughSeatsException.create(ticket);
        }
        return ticket;
    }
    
    @Transactional
    public Ticket updateTicketBusLine(Ticket ticket, BusLine busLine) {
        if (ticket != null && busLine != null) {
            ticket.setBusLine(busLine);
            if (!busLine.containsDestination(ticket.getStartingDestination()))
                ticket.setStartingDestination(busLine.getStartingDestination());
            if (!busLine.containsDestination(ticket.getFinalDestination()))
                ticket.setFinalDestination(busLine.getFinalDestination());
            computeLengthAndTime(ticket);
            ticket.setDateTime(new Date());
            dao.update(ticket);
            return ticket;
        } else
            return null;
    }
    
    @Transactional
    public void updateTicketStartingDestination(Ticket ticket, Destination startingDestination) {
        if (ticket != null && startingDestination != null) {
            if (ticket.getFinalDestination() == null || ticket.getBusLine().containsDestination(startingDestination)) {
                ticket.setStartingDestination(startingDestination);
            } else {
                BusLine shortest = findQuickestNextLineConnectingDestinations(startingDestination, ticket.getFinalDestination());
                if (shortest == null)
                    throw new DisconnectedDestinationsUsingBusLinesException("Given Destinations (" + startingDestination.getName() + " -> " + ticket.getFinalDestination().getName() + ") are NOT conneted with any BusLine");
                else {
                    ticket.setBusLine(shortest);
                    ticket.setStartingDestination(startingDestination);
                }
            }
            computeLengthAndTime(ticket);
            ticket.setDateTime(new Date());
            dao.update(ticket);
        }
    }
    
    @Transactional
    public void updateTicketFinalDestination(Ticket ticket, Destination finalDestination) {
        if (ticket != null && finalDestination != null) {
            if (ticket.getStartingDestination()== null || ticket.getBusLine().containsDestination(finalDestination)) {
                ticket.setFinalDestination(finalDestination);
            } else {
                BusLine shortest = findQuickestNextLineConnectingDestinations(ticket.getStartingDestination(), finalDestination);
                if (shortest == null)
                    throw new DisconnectedDestinationsUsingBusLinesException("Given Destinations (" + ticket.getStartingDestination().getName() + " -> " + finalDestination.getName() + ") are NOT conneted with any BusLine");
                else {
                    ticket.setBusLine(shortest);
                    ticket.setFinalDestination(finalDestination);
                }
            }
            computeLengthAndTime(ticket);
            ticket.setDateTime(new Date());
            dao.update(ticket);
        }
    }
    
    @Transactional
    public void removeTicket(Ticket ticket) {
        if (ticket != null)
            dao.remove(ticket);
    }
    
    /**
     * Computes Length and Time between Starting and Final Destinations of given Ticket, saves this infomration into given Ticket and returns time difference
     *
     * @param Ticket to compute and change
     * @return Long difference of arrival - departure
     */
    private long computeLengthAndTime(Ticket ticket) {
        Destination start = ticket.getStartingDestination();
        Destination end = ticket.getFinalDestination();
        if (!ticket.getBusLine().containsDestination(start))
            throw new InsufficientDestinationException("Destination " + start + " was not found in given BusLine!");
        if (!ticket.getBusLine().containsDestination(end))
            throw new InsufficientDestinationException("Destination " + end + " was not found in given BusLine!");
        
        List<DestinationKilometerMinutes> destinationsWithInfo = ticket.getBusLine().getDestinations();
        int startIndex = ticket.getBusLine().getDestinationIndex(start);
        int endIndex = ticket.getBusLine().getDestinationIndex(end);
        assert(startIndex > -1 && endIndex > -1);
        assert(startIndex < destinationsWithInfo.size() && endIndex < destinationsWithInfo.size());
        
        int minutes = 0;
        if (endIndex == startIndex)
            return 0;
        Date departure = ticket.getBusLine().getDateTime();
        for (int i = 0; i < startIndex; i++)
            minutes += destinationsWithInfo.get(i).getMinutesFromPrevious();
        departure = DateUtils.addMinutes(departure, minutes);
        int length = 0;
        minutes = 0;
        for (int i = startIndex + 1; i <= endIndex; i++) {
            length += destinationsWithInfo.get(i).getKilometersFromPrevious();
            minutes += destinationsWithInfo.get(i).getMinutesFromPrevious();
        }
        
        ticket.setLength(length);
        ticket.setDepartureTime(departure);
        Date arrival = DateUtils.addMinutes(departure, minutes);
        ticket.setArrivalTime(arrival);
        return arrival.getTime() - departure.getTime();
    }

    private BusLine findQuickestNextLineConnectingDestinations(Destination startingDestination, Destination finalDestination) {
        List<BusLine> allLines = busLineDao.findAll();
        BusLine shortest = null;
        long shortestTime = Long.MAX_VALUE;
        Date now = new Date();
        List<DestinationKilometerMinutes> kmmDestinations;
        int startIndex;
        int endIndex;
        long timeBetweenDestinations;
        for (BusLine bl : allLines) {
            if (bl.getDateTime().after(now) && bl.containsDestination(startingDestination) && bl.containsDestination(finalDestination)) {
                startIndex = bl.getDestinationIndex(startingDestination);
                endIndex = bl.getDestinationIndex(finalDestination);
                if (startIndex < endIndex) {
                    kmmDestinations = bl.getDestinations();
                    timeBetweenDestinations = 0;
                    for (int i = startIndex + 1; i <= endIndex; i++)
                        timeBetweenDestinations += kmmDestinations.get(i).getMinutesFromPrevious();
                    if (shortest == null || timeBetweenDestinations < shortestTime) {
                        shortest = bl;
                        shortestTime = timeBetweenDestinations;
                    }
                }
            }
        }
        return shortest;
    }
}
