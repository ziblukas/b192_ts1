package cz.cvut.fel.ear.bussystem.service;

import cz.cvut.fel.ear.bussystem.dao.BusLineDao;
import cz.cvut.fel.ear.bussystem.dao.DriverDao;
import cz.cvut.fel.ear.bussystem.dao.UserDao;
import cz.cvut.fel.ear.bussystem.model.BusLine;
import cz.cvut.fel.ear.bussystem.model.Driver;
import cz.cvut.fel.ear.bussystem.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DriverService {
    private final DriverDao driverDao;
    private final UserDao userDao;
    private final BusLineDao busLineDao;
    
    @Autowired
    public DriverService(DriverDao driverDao, UserDao userDao, BusLineDao busLineDao) {
        this.driverDao = driverDao;
        this.userDao = userDao;
        this.busLineDao = busLineDao;
    }

    @Transactional
    public Driver find(Integer id) {
        if (id == null)
            return null;
        else
            return driverDao.find(id);
    }
    
    @Transactional
    public Driver addDriver(Driver driver) {
        if (driver != null) {
            User user = userDao.findByUsername(driver.getUsername());
            if (user == null)
                return null;
            driverDao.persist(driver);
        }
        return driver;
    }
    
    @Transactional
    public void olderDriver(Driver driver) {
        if (driver != null) {
            driver.setAge(driver.getAge() + 1);
            driverDao.update(driver);
        }
    }
    
    @Transactional
    public void addMinutesDrivenInRow(Driver driver, int minutes) {
        if (driver != null && minutes >= 0) {
            driver.setMinutesDrivenInRow(driver.getMinutesDrivenInRow()+ minutes);
            driverDao.update(driver);
        }
    }
    
    @Transactional
    public void resetMinutesDrivenInRow(Driver driver) {
        if (driver != null) {
            driver.setMinutesDrivenInRow(0);
            driverDao.update(driver);
        }
    }
    
    @Transactional
    public void updateBusLineToDriver(Driver driver, BusLine line) {
        if (driver != null && line != null) {
            List<BusLine> lines;
            Driver oldDriver = line.getDriver();
            if (oldDriver != null) {
                lines = oldDriver.getLines();
                if (lines != null && lines.contains(line))
                    lines.remove(line);
                oldDriver.setLines(lines);
                driverDao.update(oldDriver);
            }
            lines = driver.getLines();
            lines.add(line);
            driver.setLines(lines);
            line.setDriver(driver);
            driverDao.update(driver);
            busLineDao.update(line);
        }
    }
    
    @Transactional
    public Driver updateEmail(Driver driver, String email) {
        if (driver != null && email != null) {
            driver.setEmail(email);
            driverDao.update(driver);
            return driver;
        } else
            return null;
    }
    
    @Transactional
    public void removeDriver(Driver driver) {
        if (driver != null)
            driverDao.remove(driver);
    }

    @Transactional
    public boolean exists(String username) {
        return userDao.exists(username);
    }
    
    @Transactional
    public User validatePassword(String username, String password) {
        User user = userDao.findByUsername(username);
        if (user != null && user.getPassword().equals(password))
            return user;
        else
            return null;
    }
}
