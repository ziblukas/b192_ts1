
package cz.cvut.fel.ear.bussystem.service;

import cz.cvut.fel.ear.bussystem.model.Administrator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PostConstruct;

@Component
public class SystemInitializer {

    private static final Logger LOG = LoggerFactory.getLogger(SystemInitializer.class);


/**
     * Default admin username
     */

    private static final String ADMIN_USERNAME = "BussystemAdmin101248";
    private static final String ADMIN_EMAIL = "ziblukas@fel.cvut.cz";

    private final AdministratorService adminService;

    private final PlatformTransactionManager txManager;

    @Autowired
    public SystemInitializer(AdministratorService adminService,
                             PlatformTransactionManager txManager) {
        this.adminService = adminService;
        this.txManager = txManager;
    }

    @PostConstruct
    private void initSystem() {
        TransactionTemplate txTemplate = new TransactionTemplate(txManager);
        txTemplate.execute((status) -> {
            generateAdmin();
            return null;
        });
    }


/**
     * Generates an admin account if it does not already exist.
     */

    private void generateAdmin() {
        if (adminService.exists(ADMIN_USERNAME)) {
            return;
        }
        final Administrator admin = new Administrator();
        admin.setUsername(ADMIN_USERNAME);
        admin.setPassword(ADMIN_USERNAME + "pass");
        admin.setLevel(AdministratorService.MAX_LEVEL);
        admin.setEmail(ADMIN_EMAIL);
        LOG.info("Generated admin user with credentials " + admin.getUsername() + "/" + admin.getPassword());
        adminService.createAdministrator(admin);
    }
}