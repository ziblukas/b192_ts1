package cz.cvut.fel.ear.bussystem.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Bus extends AbstractEntity {
    private int drivenKilometers;
    private int yearOfManufacture;
    private int seats;
    @Enumerated
    private Luggage luggageSpace;
    private String description;
}
