package cz.cvut.fel.ear.bussystem.dao;

import cz.cvut.fel.ear.bussystem.model.Ticket;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class TicketDao extends BaseDao<Ticket> {
    
    public TicketDao() {
        super(Ticket.class);
    }

    @Override
    public List<Ticket> findAll() {
                        // query: "SELECT t FROM Ticket t WHERE NOT t.removed"
        return em.createQuery("SELECT t FROM Ticket t", Ticket.class).getResultList();
    }

    @Override
    public Ticket find(Long id) {
        if (id != null)
            return em.find(Ticket.class, id);
        else
            return null;
    }
}
