package cz.cvut.fel.ear.bussystem.dao;

import cz.cvut.fel.ear.bussystem.model.Destination;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class DestinationDao extends BaseDao<Destination> {
    
    public DestinationDao() {
        super(Destination.class);
    }

    @Override
    public List<Destination> findAll() {
                        // query: "SELECT b FROM Bus b WHERE NOT b.removed"
        return em.createQuery("SELECT d FROM Destination d", Destination.class).getResultList();
    }

    public Destination find(Integer id) {
        if (id != null)
            return em.find(Destination.class, id);
        else
            return null;
    }

    public List<Destination> findConnectedDestinations(Integer id) {
        if (id != null)
            return find(id).getConnectedDestinations();
        else
            return null;
    }
}
