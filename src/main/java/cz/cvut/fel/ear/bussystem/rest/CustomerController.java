package cz.cvut.fel.ear.bussystem.rest;

import cz.cvut.fel.ear.bussystem.exceptions.ForbiddenException;
import cz.cvut.fel.ear.bussystem.exceptions.NotFoundException;
import cz.cvut.fel.ear.bussystem.model.Customer;
import cz.cvut.fel.ear.bussystem.model.Ticket;
import cz.cvut.fel.ear.bussystem.rest.util.RestUtils;
import cz.cvut.fel.ear.bussystem.security.RoleChecker;
import cz.cvut.fel.ear.bussystem.security.SecurityUtils;
import cz.cvut.fel.ear.bussystem.service.CustomerService;
import cz.cvut.fel.ear.bussystem.service.TicketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/customers")
public class CustomerController {

    private static final Logger LOG = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private CustomerService customerService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private RoleChecker roleChecker;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Customer getCustomer(@PathVariable Integer id) {
        Customer customer = customerService.find(id);
        if (customer == null)
            throw NotFoundException.create("Customer", id);
        return customer;
    }

    @PostMapping(value = "/customer", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createCustomer(@RequestBody Customer customer) {
        if (customerService.addCustomer(customer) != null) {
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", customer.getId());
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        } else
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/customer", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void removeCustomer(@RequestBody Customer customer) {
        if (customerService.find(customer.getId()) == null)
            throw NotFoundException.create("Customer", customer.getId());
        if (roleChecker.isAdmin() || (customer == SecurityUtils.getCurrentUser() && roleChecker.isCustomer()))
            customerService.removeCustomer(customer);
        else
            throw ForbiddenException.create();
    }

    @PutMapping(value = "/{customerId}/{email}")
    public Customer updateEmail(@PathVariable Integer customerId, @PathVariable String email) {
        Customer customer = customerService.find(customerId);
        if (customer == null)
            throw NotFoundException.create("Customer", customerId);
        if ((customer == SecurityUtils.getCurrentUser() && roleChecker.isCustomer()) || roleChecker.isAdmin()) {
            customerService.updateEmail(customer, email);
            return customer;
        }
        throw ForbiddenException.create();
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(value = "/{customerId}/{ticketId}")
    public void addTicketToCustomer(Integer customerId, Long ticketId) {
        Customer customer = customerService.find(customerId);
        Ticket ticket = ticketService.find(ticketId);
        if (customer == null)
            throw NotFoundException.create("Customer", customerId);
        if (ticket == null)
            throw NotFoundException.create("Ticket", ticketId);
        if (roleChecker.isAdmin() || customer == SecurityUtils.getCurrentUser())
            customerService.addTicketToCustomer(customer, ticket);
        else
            throw ForbiddenException.create();
    }
}
