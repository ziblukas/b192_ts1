package cz.cvut.fel.ear.bussystem.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;


@Entity
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "Administrator.findByUsername", query = "SELECT u FROM Administrator u WHERE u.username = :username")
})
public class Administrator extends User {
    private int level;

    public Administrator(String username, String password, String email) {
        super(username, password, email);
    }

    public Administrator() {
        super("", "", "");
    }

    @Override
    public String getRole() {
        return "ROLE_ADMINISTRATOR";
    }

    @Override
    public void erasePassword() {
        //TODO
    }
}
