package cz.cvut.fel.ear.bussystem.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
public class BusLine extends AbstractEntity {
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Bus bus;
    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "destination_id")
    private List<DestinationKilometerMinutes> destinations;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Driver driver;
    private Date dateTime;
    private Regularity regularity;
    
    public Destination getStartingDestination() {
        if (destinations.size() > 0)
            return (Destination) destinations.get(0).getDestination();
        else
            return null;
    }
    
    public Destination getFinalDestination() {
        if (destinations.size() > 0)
            return (Destination) destinations.get(destinations.size() - 1).getDestination();
        else
            return null;
    }
    
    public boolean containsDestination(Destination des) {
        if (!destinations.isEmpty()) {
            for (DestinationKilometerMinutes dkmm : destinations) {
                if (dkmm.getDestination().equals(des))
                    return true;
            }
            return false;
        } else
            return false;
    }
    
    public int getDestinationIndex(Destination des) {
        if (!destinations.isEmpty()) {
            int ret = 0;
            for (DestinationKilometerMinutes dkmm : destinations) {
                if (dkmm.getDestination().equals(des))
                    return ret;
                ret++;
            }
            return -1;
        } else
            return -1;
    }
}
