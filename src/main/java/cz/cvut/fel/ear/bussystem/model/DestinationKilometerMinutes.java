package cz.cvut.fel.ear.bussystem.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class DestinationKilometerMinutes extends AbstractEntity {
    @ManyToOne
    private BusLine busLine;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Destination destination;
    private int kilometersFromPrevious = 0;
    private int minutesFromPrevious = 0;

    public DestinationKilometerMinutes(BusLine busLine, Destination destination, int kilometersFromPrevious, int minutesFromPrevious) {
        this.busLine = busLine;
        this.destination = destination;
        this.kilometersFromPrevious = kilometersFromPrevious;
        this.minutesFromPrevious = minutesFromPrevious;
    }

    public DestinationKilometerMinutes(BusLine busLine, Destination destination) {
        this.busLine = busLine;
        this.destination = destination;
        this.kilometersFromPrevious = 0;
        this.minutesFromPrevious = 0;
    }

    // DO NOT USE
    public DestinationKilometerMinutes() {
        this.busLine = null;
        this.destination = null;
        this.kilometersFromPrevious = 0;
        this.minutesFromPrevious = 0;
    }
}
