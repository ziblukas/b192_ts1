package cz.cvut.fel.ear.bussystem.rest;

import cz.cvut.fel.ear.bussystem.exceptions.DisconnectedDestinationsUsingBusLinesException;
import cz.cvut.fel.ear.bussystem.exceptions.ForbiddenException;
import cz.cvut.fel.ear.bussystem.exceptions.NotFoundException;
import cz.cvut.fel.ear.bussystem.model.BusLine;
import cz.cvut.fel.ear.bussystem.model.Destination;
import cz.cvut.fel.ear.bussystem.model.Ticket;
import cz.cvut.fel.ear.bussystem.rest.util.RestUtils;
import cz.cvut.fel.ear.bussystem.security.RoleChecker;
import cz.cvut.fel.ear.bussystem.service.TicketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/tickets")
public class TicketController {

    private static final Logger LOG = LoggerFactory.getLogger(TicketController.class);

    @Autowired
    private TicketService ticketService;

    @Autowired
    private RoleChecker roleChecker;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Ticket getTicket(@PathVariable Long id) {
        final Ticket ticket = ticketService.find(id);
        if (ticket == null)
            throw NotFoundException.create("Ticket", id);
        return ticket;
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createTicket(@RequestBody Ticket ticket) {
        if (roleChecker.isCustomer()) {
            ticketService.addTicket(ticket);
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", ticket.getId());
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
    }

    @PutMapping(value = "/updateline", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> updateTicketBusLine(@RequestBody BusLine busLine, @RequestBody Ticket ticket) {
        if (roleChecker.isCustomer()) {
            if (ticketService.updateTicketBusLine(ticket, busLine) != null)
                return new ResponseEntity<>(null, HttpStatus.ACCEPTED);
            else
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        throw ForbiddenException.create();
    }

    @PutMapping(value = "/updatesdest", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> updateTicketStartingDestination(@RequestBody Destination startingDestination, @RequestBody Ticket ticket) {
        if (roleChecker.isCustomer()) {
            try {
                ticketService.updateTicketStartingDestination(ticket, startingDestination);
                return new ResponseEntity<>(null, HttpStatus.ACCEPTED);
            } catch (DisconnectedDestinationsUsingBusLinesException e) {
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
            }
        }
        throw ForbiddenException.create();
    }

    @PutMapping(value = "/updatefdest", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> updateTicketFinalDestination(@RequestBody Destination finalDestination, @RequestBody Ticket ticket) {
        if (roleChecker.isCustomer()) {
            try {
                ticketService.updateTicketFinalDestination(ticket, finalDestination);
                return new ResponseEntity<>(null, HttpStatus.ACCEPTED);
            } catch (DisconnectedDestinationsUsingBusLinesException e) {
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
            }
        }
        throw ForbiddenException.create();
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/ticket", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void removeTicket(@RequestBody Ticket ticket) {
        if (ticketService.find(ticket.getId()) == null)
            throw NotFoundException.create("Ticket", ticket.getId());
        if (roleChecker.isAdmin() || roleChecker.isCustomer())
            ticketService.removeTicket(ticket);
        else
            throw ForbiddenException.create();
    }
}
