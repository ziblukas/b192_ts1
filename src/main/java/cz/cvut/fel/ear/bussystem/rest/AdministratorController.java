package cz.cvut.fel.ear.bussystem.rest;

import cz.cvut.fel.ear.bussystem.exceptions.ForbiddenException;
import cz.cvut.fel.ear.bussystem.exceptions.NotFoundException;
import cz.cvut.fel.ear.bussystem.model.Administrator;
import cz.cvut.fel.ear.bussystem.rest.util.RestUtils;
import cz.cvut.fel.ear.bussystem.security.RoleChecker;
import cz.cvut.fel.ear.bussystem.service.AdministratorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/administrators")
public class AdministratorController {

    private static final Logger LOG = LoggerFactory.getLogger(AdministratorController.class);

    @Autowired
    private AdministratorService administratorService;

    //Because tests are not using my bean and i dont know why
    private RoleChecker roleChecker = new RoleChecker();

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Administrator getAdministrator(@PathVariable Integer id) {
        Administrator admin = administratorService.find(id);
        if (admin == null)
            throw NotFoundException.create("Administrator", id);
        return admin;
    }

    @PostMapping(value = "/admin", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> creteAdministrator(@RequestBody Administrator administrator) {
        if (roleChecker.isAdmin()) {
            if (administratorService.createAdministrator(administrator) != null) {
                final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", administrator.getId());
                return new ResponseEntity<>(headers, HttpStatus.CREATED);
            } else
                return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
    }

    @PutMapping(value = "/promote/{adminToPromoteId}/{level}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Administrator promoteAdministrator(@RequestBody Administrator administrator, @PathVariable Integer adminToPromoteId, @PathVariable Integer level) {
        if (roleChecker.isAdmin())
            return administratorService.promoteAdministrator(administrator, adminToPromoteId, level);
        throw ForbiddenException.create();
    }

    @PutMapping(value = "/demote/{adminToDemoteId}/{level}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Administrator demoteAdministrator(@RequestBody Administrator administrator, @PathVariable Integer adminToDemoteId, @PathVariable Integer level) {
        if (roleChecker.isAdmin())
            return administratorService.demoteAdministrator(administrator, adminToDemoteId, level);
        throw ForbiddenException.create();
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/admin", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void removeAdministrator(@RequestBody Administrator administrator) {
        if (administratorService.find(administrator.getId()) == null)
            throw NotFoundException.create("Administrator", administrator.getId());
        if (roleChecker.isAdmin())
            administratorService.removeAdministrator(administrator);
        else
            throw ForbiddenException.create();
    }

    @PutMapping(value = "/{administratorId}/{email}")
    public Administrator updateEmail(@PathVariable Integer administratorId, @PathVariable String email) {
        Administrator administrator = administratorService.find(administratorId);
        if (administrator == null)
            throw NotFoundException.create("Administrator", administratorId);
        if (roleChecker.isAdmin()) {
            administratorService.updateEmail(administrator, email);
            return administrator;
        }
        throw ForbiddenException.create();
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/admin/{toDeleteId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void removeUser(@RequestBody Administrator administrator, @PathVariable Integer toDeleteId) {
        if (administratorService.find(toDeleteId) == null)
            throw NotFoundException.create("Administrator", toDeleteId);
        if (roleChecker.isAdmin())
            administratorService.deleteUser(administrator, toDeleteId);
        else
            throw ForbiddenException.create();
    }

}
