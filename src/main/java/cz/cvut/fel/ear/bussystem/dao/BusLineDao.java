package cz.cvut.fel.ear.bussystem.dao;

import cz.cvut.fel.ear.bussystem.model.BusLine;
import cz.cvut.fel.ear.bussystem.model.Destination;
import cz.cvut.fel.ear.bussystem.model.DestinationKilometerMinutes;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class BusLineDao extends BaseDao<BusLine> {
    
    public BusLineDao() {
        super(BusLine.class);
    }

    @Override
    public List<BusLine> findAll() {
                        // query: "SELECT l FROM BusLine l WHERE NOT b.removed"          JOIN Driver d ON (l.driver = d.id) JOIN Bus b ON (l.bus = b.id)
        return em.createQuery("SELECT l FROM BusLine l", BusLine.class).getResultList();
    }

    @Override
    public BusLine find(Integer id) {
        if (id != null)
            return em.find(BusLine.class, id);
        else
            return null;
    }

    public List<Destination> findDestinations(Integer id) {
        if (id != null) {
            List<Destination> ret = new ArrayList<>();
            for (DestinationKilometerMinutes dkmm : find(id).getDestinations()) {
                ret.add(dkmm.getDestination());
            }
            return ret;
        } else
            return null;
    }
}
