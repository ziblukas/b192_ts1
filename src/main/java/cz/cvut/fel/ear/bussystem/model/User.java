package cz.cvut.fel.ear.bussystem.model;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
@AllArgsConstructor
public abstract class User extends AbstractEntity {
    @Column(unique = true)
    private String username;
    private String password;
    private String email;

    public abstract String getRole();

    //TODO
    public abstract void erasePassword();
}
