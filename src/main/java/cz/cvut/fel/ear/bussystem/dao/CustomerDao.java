package cz.cvut.fel.ear.bussystem.dao;

import cz.cvut.fel.ear.bussystem.model.Customer;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerDao extends BaseDao<Customer> {
    
    public CustomerDao() {
        super(Customer.class);
    }
    
    public Customer findByUsername(String username) {
        if (username != null) {
            try {
                return em.createNamedQuery("Customer.findByUsername", Customer.class).setParameter("username", username)
                         .getSingleResult();
            } catch (NoResultException e) {
                return null;
            }
        } else
            return null;
    }
}
