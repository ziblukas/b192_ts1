package cz.cvut.fel.ear.bussystem.service;

import cz.cvut.fel.ear.bussystem.dao.AdministratorDao;
import cz.cvut.fel.ear.bussystem.dao.UserDao;
import cz.cvut.fel.ear.bussystem.exceptions.PersistenceException;
import cz.cvut.fel.ear.bussystem.exceptions.ValidationException;
import cz.cvut.fel.ear.bussystem.model.Administrator;
import cz.cvut.fel.ear.bussystem.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AdministratorService {
    public static final int MAX_LEVEL = 5;
    public static final int MIN_LEVEL = 1;
    private final AdministratorDao admindao;
    private final UserDao userDao;
    
    @Autowired
    public AdministratorService(AdministratorDao admindao, UserDao userDao) {
        this.admindao = admindao;
        this.userDao = userDao;
    }

    @Transactional
    public Administrator find(Integer id) {
        return admindao.find(id);
    }
    
    @Transactional
    public Administrator createAdministrator(Administrator admin) {
        if (admin != null) {
            User user = userDao.findByUsername(admin.getUsername());
            if (user == null)
                return null;
            if (admin.getLevel() < MIN_LEVEL)
                admin.setLevel(MIN_LEVEL);
            else if (admin.getLevel() > MAX_LEVEL)
                admin.setLevel(MAX_LEVEL);
            admindao.persist(admin);
        }
        return admin;
    }
    
    @Transactional
    public Administrator promoteAdministrator(Administrator loggedAdmin, int toPromoteId, int levels) {
        if (loggedAdmin != null && levels >= 0 && admindao.exists(toPromoteId)) {
            Administrator toPromote = admindao.find(toPromoteId);

            if (toPromote.getLevel() > loggedAdmin.getLevel())
                throw new ValidationException("Level of the logged Administrator is too low to promote Administrator with id: " + toPromoteId);
            if (!toPromote.getId().equals(loggedAdmin.getId())) {
                if (toPromote.getLevel() + levels > loggedAdmin.getLevel())
                    toPromote.setLevel(loggedAdmin.getLevel() + 1);
                else
                    toPromote.setLevel(toPromote.getLevel() + levels);
                if (toPromote.getLevel() > MAX_LEVEL)
                    toPromote.setLevel(MAX_LEVEL);
                admindao.update(toPromote);
            }
            return toPromote;
        } else
            return null;
    }
    
    @Transactional
    public Administrator demoteAdministrator(Administrator loggedAdmin, int toDemoteId, int levels) {
        if (loggedAdmin != null && levels >= 0 && admindao.exists(toDemoteId)) {
            Administrator toDemote = admindao.find(toDemoteId);

            if (toDemote.getLevel() > loggedAdmin.getLevel())
                throw new ValidationException("Level of the logged Administrator is too low to demote Administrator with id: " + toDemoteId);
            if (!toDemote.getId().equals(loggedAdmin.getId())) {
                toDemote.setLevel(toDemote.getLevel() - levels);
                if (toDemote.getLevel() < MIN_LEVEL)
                    toDemote.setLevel(MIN_LEVEL);
                admindao.update(toDemote);
            }
            return toDemote;
        } else
            return null;
    }
    
    @Transactional
    public Administrator updateEmail(Administrator admin, String email) {
        if (admin != null && email != null) {
            admin.setEmail(email);
            admindao.update(admin);
            return admin;
        } else
            return null;
    }
    
    @Transactional
    public void removeAdministrator(Administrator admin) {
        if (admin != null)
            admindao.remove(admin);
    }

    @Transactional
    public boolean exists(String username) {
        return userDao.exists(username);
    }
    
    @Transactional
    public User validatePassword(String username, String password) {
        User user = userDao.findByUsername(username);
        if (user != null && user.getPassword().equals(password))
            return user;
        else
            return null;
    }
    
    @Transactional
    public boolean deleteUser(Administrator loggedAdmin, int toDeleteId) throws ValidationException {
        if (loggedAdmin != null) {
            try {
                if (admindao.exists(toDeleteId) && admindao.find(toDeleteId).getLevel() >= loggedAdmin.getLevel())
                    throw new ValidationException("Level of the logged Administrator is too low to delete Administrator with id: " + toDeleteId);
                userDao.remove(userDao.find(toDeleteId));
                return true;
            } catch (PersistenceException e) {
                return false;
            }
        } else
            return false;
    }
}
