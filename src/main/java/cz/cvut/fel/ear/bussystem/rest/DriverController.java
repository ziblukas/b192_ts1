package cz.cvut.fel.ear.bussystem.rest;

import cz.cvut.fel.ear.bussystem.exceptions.ForbiddenException;
import cz.cvut.fel.ear.bussystem.exceptions.NotFoundException;
import cz.cvut.fel.ear.bussystem.model.BusLine;
import cz.cvut.fel.ear.bussystem.model.Driver;
import cz.cvut.fel.ear.bussystem.rest.util.RestUtils;
import cz.cvut.fel.ear.bussystem.security.RoleChecker;
import cz.cvut.fel.ear.bussystem.security.SecurityUtils;
import cz.cvut.fel.ear.bussystem.service.BusLineService;
import cz.cvut.fel.ear.bussystem.service.DriverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/drivers")
public class DriverController {

    private static final Logger LOG = LoggerFactory.getLogger(DriverController.class);

    @Autowired
    private DriverService driverService;

    @Autowired
    private BusLineService busLineService;

    @Autowired
    private RoleChecker roleChecker;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Driver getDriver(@PathVariable Integer id) {
        Driver driver = driverService.find(id);
        if (driver == null)
            throw NotFoundException.create("Driver", id);
        return driver;
    }

    @PostMapping(value = "/driver", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createDriver(@RequestBody Driver driver) {
        if (roleChecker.isAdmin()) {
            if (driverService.addDriver(driver) != null) {
                final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", driver.getId());
                return new ResponseEntity<>(headers, HttpStatus.CREATED);
            } else
                return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/driver", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void removeDriver(@RequestBody Driver driver) {
        if (driverService.find(driver.getId()) == null)
            throw NotFoundException.create("Driver", driver.getId());
        if (roleChecker.isAdmin())
            driverService.removeDriver(driver);
        else
            throw ForbiddenException.create();
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(value = "/{driverId}/{lineId}")
    public void updateBusLineToDriver(@PathVariable Integer driverId, @PathVariable Integer lineId) {
        Driver driver = driverService.find(driverId);
        BusLine line = busLineService.find(lineId);
        if (driver == null)
            throw NotFoundException.create("Driver", driverId);
        if (line == null)
            throw NotFoundException.create("Line", lineId);
        if (roleChecker.isAdmin())
            driverService.updateBusLineToDriver(driver, line);
        else
            throw ForbiddenException.create();
    }

    @PutMapping(value = "/{driverId}/{email}")
    public Driver updateEmail(@PathVariable Integer driverId, @PathVariable String email) {
        Driver driver = driverService.find(driverId);
        if (driver == null)
            throw NotFoundException.create("Driver", driverId);
        if ((driver == SecurityUtils.getCurrentUser() && roleChecker.isDriver()) || roleChecker.isAdmin()) {
            driverService.updateEmail(driver, email);
            return driver;
        }
        throw ForbiddenException.create();
    }
}
