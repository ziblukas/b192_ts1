package cz.cvut.fel.ear.bussystem.service;

import cz.cvut.fel.ear.bussystem.dao.BusLineDao;
import cz.cvut.fel.ear.bussystem.dao.DriverDao;
import cz.cvut.fel.ear.bussystem.model.BusLine;
import cz.cvut.fel.ear.bussystem.model.DestinationKilometerMinutes;
import cz.cvut.fel.ear.bussystem.model.Driver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class BusLineService {
    private final BusLineDao busLineDao;
    private final DriverDao driverDao;
    
    @Autowired
    public BusLineService(BusLineDao busLineDao, DriverDao driverDao) {
        this.busLineDao = busLineDao;
        this.driverDao = driverDao;
    }

    @Transactional
    public BusLine find(Integer id) {
        return busLineDao.find(id);
    }
    
    @Transactional
    public BusLine addBusLine(BusLine busLine) {
        if (busLine != null) {
            for (DestinationKilometerMinutes dkmm : busLine.getDestinations()) {
                dkmm.setBusLine(busLine);
            }
            busLineDao.persist(busLine);
        }
        return busLine;
    }
    
    @Transactional
    public BusLine updateDriverOfBusLine(BusLine line, Driver newDriver) {
        if (line != null && newDriver != null) {
            Driver oldDriver = line.getDriver();
            List<BusLine> lines;
            if (oldDriver != null) {
                lines = oldDriver.getLines();
                lines.remove(line);
                oldDriver.setLines(lines);
                driverDao.update(oldDriver);
            }
            lines = newDriver.getLines();
            lines.add(line);
            newDriver.setLines(lines);
            line.setDriver(newDriver);
            driverDao.update(newDriver);
            busLineDao.update(line);
            return line;
        } else
            return null;
    }
    
    @Transactional
    public BusLine updateBusLineDateTime(BusLine busLine, Date dateTime) {
        if (busLine != null && dateTime != null) {
            busLine.setDateTime(dateTime);
            busLineDao.update(busLine);
            return busLine;
        } else
            return null;
    }
    
    @Transactional
    public void removeBusLine(BusLine busLine) {
        if (busLine != null)
            busLineDao.remove(busLine);
    }
}
