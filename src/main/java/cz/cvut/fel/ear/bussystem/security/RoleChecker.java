package cz.cvut.fel.ear.bussystem.security;

public class RoleChecker {

    public boolean isAdmin() {
        return (!SecurityUtils.isAuthenticatedAnonymously() && SecurityUtils.getCurrentUser().getRole().equals("ROLE_ADMINISTRATOR"));
    }

    public boolean isAdminOrDriver() {
        return (!SecurityUtils.isAuthenticatedAnonymously() && (SecurityUtils.getCurrentUser().getRole().equals("ROLE_ADMINISTRATOR") || SecurityUtils.getCurrentUser().getRole().equals("ROLE_DRIVER")));
    }

    public boolean isDriver() {
        return (!SecurityUtils.isAuthenticatedAnonymously() && SecurityUtils.getCurrentUser().getRole().equals("ROLE_DRIVER"));
    }

    public boolean isCustomer() {
        return (!SecurityUtils.isAuthenticatedAnonymously() && SecurityUtils.getCurrentUser().getRole().equals("ROLE_CUSTOMER"));
    }

    public boolean isLogged() {
        return (!SecurityUtils.isAuthenticatedAnonymously());
    }
}
