package cz.cvut.fel.ear.bussystem.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.Date;

@Configuration
public class DateConfig {
    @Bean
    @Scope("prototype")
    public Date createDate(){
        return new Date();
    }
}
