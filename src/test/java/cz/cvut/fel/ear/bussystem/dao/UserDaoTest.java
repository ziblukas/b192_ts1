package cz.cvut.fel.ear.bussystem.dao;

import cz.cvut.fel.ear.bussystem.BusSystemApplication;
import cz.cvut.fel.ear.bussystem.environment.*;
import cz.cvut.fel.ear.bussystem.model.Administrator;
import cz.cvut.fel.ear.bussystem.model.Customer;
import cz.cvut.fel.ear.bussystem.model.Driver;
import cz.cvut.fel.ear.bussystem.model.User;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

// Spring runner for JUnit
@RunWith(SpringRunner.class)
// DataJpaTest does not load all the application beans, it starts only persistence-related stuff
@DataJpaTest
// Exclude SystemInitializer from the startup, we don't want the admin account here
@ComponentScan(basePackageClasses = BusSystemApplication.class)
public class UserDaoTest {   
    @PersistenceContext
    protected EntityManager em;
    @Autowired
    private UserDao userDao;
    //private List<User> cleanUpUsers;
    
    public UserDaoTest() {
    }
    
/*    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        cleanUpUsers = new ArrayList<>();
    }
    
    @After
    public void tearDown() {
        if (!cleanUpUsers.isEmpty()) {
            for (User user : cleanUpUsers) {
                em.remove(user);
            }
        }
    }*/

    /**
     * Test of findByUsername method, of class UserDao.
     * Testing with:    null
     * Should return:   null
     */
    @Test
    public void testFindByUsername_nullUsername_returnsNull() {
        System.out.println("findByUsername");
        String username = null;
        User expResult = null;
        
        User result = (User) userDao.findByUsername(username);
        assertEquals(expResult, result);
    }

    /**
     * Test of exists method, of class UserDao.
     * Testing with:    new Driver with minimum info
     * Should return:   created Driver
     */
    @Test
    public void testFindByUsername_driverUsername_returnsCreatedDriver() {
        System.out.println("findByUsername");
        Driver driver = Generator.generateDriver();
        String username = driver.getUsername();
        em.persist(driver);
        //cleanUpUsers.add(driver);
        User expResult = driver;
        
        User result = (User) userDao.findByUsername(username);
        assertEquals(expResult, result);
    }

    /**
     * Test of exists method, of class UserDao.
     * Testing with:    new Customer with minimum info
     * Should return:   created Customer
     */
    @Test
    public void testFindByUsername_customerUsername_returnsCreatedCustomer() {
        System.out.println("findByUsername");
        Customer customer = Generator.generateCustomer();
        String username = customer.getUsername();
        em.persist(customer);
        //cleanUpUsers.add(customer);
        User expResult = customer;
        
        User result = (User) userDao.findByUsername(username);
        assertEquals(expResult, result);
    }

    /**
     * Test of exists method, of class UserDao.
     * Testing with:    new Administrator with minimum info
     * Should return:   created Administrator
     */
    @Test
    public void testFindByUsername_adminUsername_returnsCreatedAdmin() {
        System.out.println("findByUsername");
        Administrator admin = Generator.generateAdmin();
        String username = admin.getUsername();
        em.persist(admin);
        //cleanUpUsers.add(admin);
        User expResult = admin;
        
        User result = (User) userDao.findByUsername(username);
        assertEquals(expResult, result);
    }

    /**
     * Test of exists method, of class UserDao.
     * Testing with:    null
     * Should return:   false
     */
    @Test
    public void testExists_nullUsername_returnsFalse() {
        System.out.println("exists");
        String username = null;
        
        boolean result = userDao.exists(username);
        assertFalse(result);
    }

    /**
     * Test of exists method, of class UserDao.
     * Testing with:    new Driver with minimum info
     * Should return:   true
     */
    @Test
    public void testExists_driverUsername_returnsTrue() {
        System.out.println("exists");
        Driver driver = Generator.generateDriver();
        String username = driver.getUsername();
        em.persist(driver);
        //cleanUpUsers.add(driver);
        
        boolean result = userDao.exists(username);
        assertTrue(result);
    }

    /**
     * Test of exists method, of class UserDao.
     * Testing with:    new Customer with minimum info
     * Should return:   true
     */
    @Test
    public void testExists_customerUsername_returnsTrue() {
        System.out.println("exists");
        Customer customer = Generator.generateCustomer();
        String username = customer.getUsername();
        em.persist(customer);
        //cleanUpUsers.add(customer);
        
        boolean result = userDao.exists(username);
        assertTrue(result);
    }

    /**
     * Test of exists method, of class UserDao.
     * Testing with:    new Administrator with minimum info
     * Should return:   true
     */
    @Test
    public void testExists_adminUsername_returnsTrue() {
        System.out.println("exists");
        Administrator admin = Generator.generateAdmin();
        String username = admin.getUsername();
        em.persist(admin);
        //cleanUpUsers.add(admin);
        
        boolean result = userDao.exists(username);
        assertTrue(result);
    }    
}
