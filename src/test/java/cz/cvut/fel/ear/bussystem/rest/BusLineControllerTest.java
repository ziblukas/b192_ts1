package cz.cvut.fel.ear.bussystem.rest;

import cz.cvut.fel.ear.bussystem.service.BusLineService;
import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class BusLineControllerTest extends BaseControllerTestRunner {

    @Mock
    private BusLineService busLineServiceMock;

    @InjectMocks
    private BusLineController busLineController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        super.setUp(busLineController);
    }

}
