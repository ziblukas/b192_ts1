package cz.cvut.fel.ear.bussystem.rest;

import cz.cvut.fel.ear.bussystem.environment.*;
import cz.cvut.fel.ear.bussystem.model.Administrator;
import cz.cvut.fel.ear.bussystem.model.Bus;
import cz.cvut.fel.ear.bussystem.rest.handler.ErrorInfo;
import cz.cvut.fel.ear.bussystem.security.SecurityUtils;
import cz.cvut.fel.ear.bussystem.security.model.UserDetails;
import cz.cvut.fel.ear.bussystem.service.BusService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BusControllerTest extends BaseControllerTestRunner {

    @Mock
    private BusService busServiceMock;

    @InjectMocks
    private BusController busController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        super.setUp(busController);
        SecurityUtils.setCurrentUser(new UserDetails(new Administrator()));
    }

    @Test
    public void getByIdThrowsNotFoundForUnknownBusId() throws Exception {
        final int id = 123;
        final MvcResult mvcResult = mockMvc.perform(get("/rest/busses/" + id)).andExpect(status().isNotFound())
                .andReturn();
        final ErrorInfo result = readValue(mvcResult, ErrorInfo.class);
        assertNotNull(result);
        assertThat(result.getMessage(), containsString("Bus identified by 123 not found."));
        assertThat(result.getMessage(), containsString(Integer.toString(id)));
    }

    @Test
    public void getByIdReturnsMatchingBus() throws Exception {
        final Bus bus = new Bus();
        bus.setId(Generator.randomInt());
        bus.setSeats(100);
        when(busServiceMock.find(bus.getId())).thenReturn(bus);
        final MvcResult mvcResult = mockMvc.perform(get("/rest/busses/" + bus.getId())).andReturn();

        final Bus result = readValue(mvcResult, Bus.class);
        assertNotNull(result);
        assertEquals(bus.getId(), result.getId());
        assertEquals(bus.getSeats(), result.getSeats());
    }

    @Test
    public void createBusCreatesBusUsingService() throws Exception {
        final Bus toCreate = new Bus();
        toCreate.setSeats(100);
        mockMvc.perform(post("/rest/busses/bus").content(toJson(toCreate)).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());
        final ArgumentCaptor<Bus> captor = ArgumentCaptor.forClass(Bus.class);
        verify(busServiceMock).addBus(captor.capture());
        assertEquals(toCreate.getSeats(), captor.getValue().getSeats());
    }

    @Test
    public void createBusReturnsResponseWithLocationHeader() throws Exception {
        final Bus toCreate = new Bus();
        toCreate.setSeats(100);
        toCreate.setId(Generator.randomInt());
        final MvcResult mvcResult = mockMvc
                .perform(post("/rest/busses/bus").content(toJson(toCreate)).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated()).andReturn();
        verifyLocationEquals("/rest/busses/bus/" + toCreate.getId(), mvcResult);
    }

    @Test
    public void removeBusRemovesBus() throws Exception {
        final Bus bus = new Bus();
        bus.setSeats(100);
        busServiceMock.addBus(bus);
        when(busServiceMock.find(any())).thenReturn(bus);
        mockMvc.perform(delete("/rest/busses/bus").content(toJson(bus)).contentType(
                MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isNoContent());
    }

    @Test
    public void removeAdministratorThrowsBadRequestForNullAdministrator() throws Exception {
        mockMvc.perform(delete("/rest/busses/bus/").content(toJson(null)).contentType(
                MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isBadRequest());
    }

}
