package cz.cvut.fel.ear.bussystem.dao;

import cz.cvut.fel.ear.bussystem.BusSystemApplication;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

// Spring runner for JUnit
@RunWith(SpringRunner.class)
// DataJpaTest does not load all the application beans, it starts only persistence-related stuff
@DataJpaTest
// Exclude SystemInitializer from the startup, we don't want the admin account here
@ComponentScan(basePackageClasses = BusSystemApplication.class)
public class DaoTest {
    
    @Autowired
    protected AdministratorDao adminDao;
    @Autowired
    protected BusDao busDao;
    @Autowired
    protected BusLineDao busLineDao;
    @Autowired
    protected CustomerDao customerDao;
    @Autowired
    protected DestinationDao destinationDao;
    @Autowired
    protected DriverDao driverDao;
    @Autowired
    protected TicketDao ticketDao;
    @Autowired
    protected UserDao userDao;

    /**
     * This test checks if the spring application contexts contains all the DAOs from the cz.cvut.fel.ear.bussystem.dao  package
     */
    @Test
    public void testRepositoriesInApplicationContext(){
        Assert.assertNotNull(adminDao);
        Assert.assertNotNull(busDao);
        Assert.assertNotNull(busLineDao);
        Assert.assertNotNull(customerDao);
        Assert.assertNotNull(destinationDao);
        Assert.assertNotNull(driverDao);
        Assert.assertNotNull(ticketDao);
        Assert.assertNotNull(userDao);
    }
}
