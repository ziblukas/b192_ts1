package cz.cvut.fel.ear.bussystem.dao;

import cz.cvut.fel.ear.bussystem.BusSystemApplication;
import cz.cvut.fel.ear.bussystem.environment.*;
import cz.cvut.fel.ear.bussystem.model.Customer;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

// Spring runner for JUnit
@RunWith(SpringRunner.class)
// DataJpaTest does not load all the application beans, it starts only persistence-related stuff
@DataJpaTest
// Exclude SystemInitializer from the startup, we don't want the admin account here
@ComponentScan(basePackageClasses = BusSystemApplication.class)
public class CustomerDaoTest {
    @PersistenceContext
    protected EntityManager em;
    @Autowired
    private CustomerDao customerDao;
    //private List<Customer> cleanUpCustomers;
    
    public CustomerDaoTest() {
    }
    
    /*@Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        cleanUpCustomers = new ArrayList<>();
    }
    
    @After
    public void tearDown() {
        if (!cleanUpCustomers.isEmpty()) {
            for (Customer customer : cleanUpCustomers) {
                em.remove(customer);
            }
        }
    }*/

    /**
     * Test of findByUsername method, of class CustomerDao.
     * Testing with:    null
     * Should return:   null
     */
    @Test
    public void testFindByUsername_nullUsername_returnsNull() {
        System.out.println("findByUsername");
        String username = null;
        Customer expResult = null;
        
        Customer result = customerDao.findByUsername(username);
        assertEquals(expResult, result);
    }

    /**
     * Test of exists method, of class CustomerDao.
     * Testing with:    new Customer with minimum info
     * Should return:   created Customer
     */
    @Test
    public void testFindByUsername_customerUsername_returnsCreatedCustomer() {
        System.out.println("findByUsername");
        Customer customer = Generator.generateCustomer();
        String username = customer.getUsername();
        em.persist(customer);
        //cleanUpCustomers.add(customer);
        Customer expResult = customer;
        
        Customer result = customerDao.findByUsername(username);
        assertNotNull(result);
        assertEquals(expResult.getId(), result.getId());
    }
}
