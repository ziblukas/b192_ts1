package cz.cvut.fel.ear.bussystem.rest;

import cz.cvut.fel.ear.bussystem.BusSystemApplication;
import cz.cvut.fel.ear.bussystem.environment.*;
import cz.cvut.fel.ear.bussystem.model.Administrator;
import cz.cvut.fel.ear.bussystem.model.User;
import cz.cvut.fel.ear.bussystem.rest.handler.ErrorInfo;
import cz.cvut.fel.ear.bussystem.security.RoleChecker;
import cz.cvut.fel.ear.bussystem.security.SecurityUtils;
import cz.cvut.fel.ear.bussystem.security.model.UserDetails;
import cz.cvut.fel.ear.bussystem.service.AdministratorService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// Spring runner for JUnit
@RunWith(SpringRunner.class)
// DataJpaTest does not load all the application beans, it starts only persistence-related stuff
@DataJpaTest
// Exclude SystemInitializer from the startup, we don't want the admin account here
@ComponentScan(basePackageClasses = BusSystemApplication.class)
public class AdministratorControllerTest extends BaseControllerTestRunner {

    @Mock
    private AdministratorService administratorServiceMock;

    @InjectMocks
    private AdministratorController administratorController;

    private RoleChecker roleChecker = new RoleChecker();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        super.setUp(administratorController);
        SecurityUtils.setCurrentUser(new UserDetails(new Administrator()));
    }

    @Test
    public void getByIdThrowsNotFoundForUnknownAdministratorId() throws Exception {
        final int id = 123;
        final MvcResult mvcResult = mockMvc.perform(get("/rest/administrators/" + id)).andExpect(status().isNotFound())
                .andReturn();
        final ErrorInfo result = readValue(mvcResult, ErrorInfo.class);
        assertNotNull(result);
        assertThat(result.getMessage(), containsString("Administrator identified by 123 not found."));
        assertThat(result.getMessage(), containsString(Integer.toString(id)));
    }

    @Test
    public void getByIdReturnsMatchingAdministrator() throws Exception {
        final Administrator administrator = new Administrator();
        administrator.setId(Generator.randomInt());
        administrator.setUsername("admin");
        when(administratorServiceMock.find(administrator.getId())).thenReturn(administrator);
        final MvcResult mvcResult = mockMvc.perform(get("/rest/administrators/" + administrator.getId())).andReturn();

        final Administrator result = readValue(mvcResult, Administrator.class);
        assertNotNull(result);
        assertEquals(administrator.getId(), result.getId());
        assertEquals(administrator.getUsername(), result.getUsername());
    }

    @Test
    public void createAdministratorCreatesAdministratorUsingService() throws Exception {
        final Administrator toCreate = new Administrator();
        toCreate.setUsername("admin");
        mockMvc.perform(post("/rest/administrators/admin").content(toJson(toCreate)).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());
        final ArgumentCaptor<Administrator> captor = ArgumentCaptor.forClass(Administrator.class);
        verify(administratorServiceMock).createAdministrator(captor.capture());
        assertEquals(toCreate.getUsername(), captor.getValue().getUsername());
    }

    @Test
    public void createAdministratorReturnsResponseWithLocationHeader() throws Exception {
        final Administrator toCreate = new Administrator();
        toCreate.setUsername("admin");
        toCreate.setId(Generator.randomInt());
        final MvcResult mvcResult = mockMvc
                .perform(post("/rest/administrators/admin").content(toJson(toCreate)).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated()).andReturn();
        verifyLocationEquals("/rest/administrators/admin/" + toCreate.getId(), mvcResult);
    }

    @Test
    public void promotedAdministratorIsReallyPromoted() throws Exception {
        final Administrator toPromote = new Administrator();
        toPromote.setLevel(1);
        toPromote.setId(Generator.randomInt());
        mockMvc.perform(put("/rest/administrators/promote/" + toPromote.getId()).content(toJson(toPromote)).contentType(
                MediaType.APPLICATION_JSON_VALUE));
        //TODO
    }

    @Test
    public void demotedAdministratorIsReallyDemoted() throws Exception {
        final Administrator toDemote = new Administrator();
        toDemote.setLevel(1);
        toDemote.setId(Generator.randomInt());
        mockMvc.perform(put("/rest/administrators/demote/" + toDemote.getId()).content(toJson(toDemote)).contentType(
                MediaType.APPLICATION_JSON_VALUE));
        //TODO
    }

    @Test
    public void removeAdministratorRemovesAdministrator() throws Exception {
        final Administrator administrator = new Administrator();
        administrator.setUsername("test");
        administratorServiceMock.createAdministrator(administrator);
        when(administratorServiceMock.find(any())).thenReturn(administrator);
        mockMvc.perform(delete("/rest/administrators/admin").content(toJson(administrator)).contentType(
                MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isNoContent());
    }

    @Test
    public void removeAdministratorThrowsBadRequestForNullAdministrator() throws Exception {
        mockMvc.perform(delete("/rest/administrators/admin/").content(toJson(null)).contentType(
                MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isBadRequest());
    }

}
