//package cz.cvut.fel.ear.bussystem.service.security;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.rules.ExpectedException;
//import org.springframework.security.core.context.SecurityContextHolder;
//
//import static org.junit.Assert.*;
//
//public class SecurityUtilsTest {
//
//    @Rule
//    public ExpectedException thrown = ExpectedException.none();
//
//    private User user;
//
//    @Before
//    public void setUp() {
//        this.user = Generator.generateUser();
//        user.setId(Generator.randomInt());
//    }
//
//    @After
//    public void tearDown() {
//        SecurityContextHolder.clearContext();
//    }
//
//    @Test
//    public void getCurrentUserReturnsCurrentlyLoggedInUser() {
//        Environment.setCurrentUser(user);
//        final User result = SecurityUtils.getCurrentUser();
//        assertEquals(user, result);
//    }
//
//    @Test
//    public void getCurrentUserDetailsReturnsUserDetailsOfCurrentlyLoggedInUser() {
//        Environment.setCurrentUser(user);
//        final UserDetails result = SecurityUtils.getCurrentUserDetails();
//        assertNotNull(result);
//        assertTrue(result.isEnabled());
//        assertEquals(user, result.getUser());
//    }
//
//    @Test
//    public void getCurrentUserDetailsReturnsNullIfNoUserIsLoggedIn() {
//        assertNull(SecurityUtils.getCurrentUserDetails());
//    }
//}