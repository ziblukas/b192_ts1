package cz.cvut.fel.ear.bussystem.service;

import cz.cvut.fel.ear.bussystem.dao.BusLineDao;
import cz.cvut.fel.ear.bussystem.dao.TicketDao;
import cz.cvut.fel.ear.bussystem.environment.*;
import cz.cvut.fel.ear.bussystem.model.BusLine;
import cz.cvut.fel.ear.bussystem.model.Destination;
import cz.cvut.fel.ear.bussystem.model.DestinationKilometerMinutes;
import cz.cvut.fel.ear.bussystem.model.Ticket;
import java.util.Date;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@TestPropertySource(locations = "classpath:application-test.properties")
public class TicketServiceTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    @Autowired
    private TicketDao ticketDao;
    @Autowired
    private BusLineDao busLineDao;
    @Autowired
    private TicketService ticketService;
    //private List<Ticket> cleanUpTickets;
    //private List<BusLine> cleanUpLines;
    
    public TicketServiceTest() {
    }
    
    /*@Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        cleanUpTickets = new ArrayList<>();
        cleanUpLines = new ArrayList<>();
    }
    
    @After
    public void tearDown() {
        if (!cleanUpTickets.isEmpty()) {
            for (Ticket ticket : cleanUpTickets) {
                ticketDao.remove(ticket);
            }
        }
        if (!cleanUpLines.isEmpty()) {
            for (BusLine line : cleanUpLines) {
                busLineDao.remove(line);
            }
        }
    }*/

    
    /**
     * Test of find method, of class TicketService.
     * Testing with:    null
     * Should return:   null
     */
    @Test
    public void testFind_nullId_returnsNull() {
        System.out.println("find");
        Long id = null;
        Ticket expResult = null;
        
        Ticket result = ticketService.find(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of find method, of class TicketService.
     * Testing with:    negative random long
     * Should return:   null
     */
    @Test
    public void testFind_negativeId_returnsNull() {
        System.out.println("find");
        Long id = (long) (- Math.random() * 100);
        Ticket expResult = null;
        
        Ticket result = ticketService.find(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of find method, of class TicketService.
     * Testing with:    id of created Ticket
     * Should return:   created Ticket
     */
    @Test
    public void testFind_validId_returnsTicket() {
        System.out.println("find");
        Ticket ticket = Generator.generateTicket();
        ticketDao.persist(ticket);
        //cleanUp.add(ticket);
        Long id = ticket.getId();
        Ticket expResult = ticket;
        
        Ticket result = ticketService.find(id);
        assertEquals(expResult, result);
    }

    
    /**
     * Test of addTicket method, of class TicketService.
     * Testing with:    null
     * Should return:   null
     */
    @Test
    public void testAddTicket_nullTicket_doesNotAddAnything() {
        System.out.println("addTicket");
        Ticket ticket = null;
        Ticket expResult = null;
        List<Ticket> expList = ticketDao.findAll();
        
        Ticket result = ticketService.addTicket(ticket);
        assertEquals(expResult, result);
        
        List<Ticket> resultList = ticketDao.findAll();
        assertEquals(expList, resultList);
    }

    /**
     * Test of addTicket method, of class TicketService.
     * Testing with:    new Ticket with minimum info
     * Should return:   created Bus
     */
    @Test
    public void testAddTicket_validTicket_addsTicket() {
        System.out.println("addTicket");
        Ticket ticket = Generator.generateTicket();
        Ticket expResult = ticket;
        
        Ticket result = ticketService.addTicket(ticket);
        Long id = ticket.getId();
        assertEquals(expResult, result);
        result = ticketDao.find(id);
        assertEquals(expResult, result);
        
        List<Ticket> resultList = ticketDao.findAll();
        assertTrue(resultList.contains(ticket));
        
        //cleanUp.add(ticket);
    }

    
    /**
     * Test of updateTicketBusLine method, of class TicketService.
     * Testing with:    null    generated Ticket    null
     *                  null    null                generated BusLine
     * Should return:   null    null                null
     */
    @Test
    public void testUpdateTicketBusLine_nullTicketOrBusLine_returnsNull() {
        System.out.println("updateTicketBusLine");
        Ticket ticket = null;
        BusLine busLine = null;
        Ticket expResult = null;
        
        Ticket result = ticketService.updateTicketBusLine(ticket, busLine);
        assertEquals(expResult, result);
        
        ticket = Generator.generateTicket();
        ticketDao.persist(ticket);
        busLine = null;
        result = ticketService.updateTicketBusLine(ticket, busLine);
        assertEquals(expResult, result);
        
        ticketDao.remove(ticket);
        ticket = null;
        busLine = Generator.generateBusLine();
        busLineDao.persist(busLine);
        result = ticketService.updateTicketBusLine(ticket, busLine);
        assertEquals(expResult, result);
    }

    
    /**
     * Test of updateTicketBusLine method, of class TicketService.
     * Testing with:    generated Ticket, generated BusLine
     * Should return:   generated Ticket with generated BusLine
     */
    @Test
    public void testUpdateTicketBusLine_validTicketAndBusLine_returnsTicket() {
        System.out.println("updateTicketBusLine");
        Ticket ticket = Generator.generateTicket();
        BusLine busLine = Generator.generateBusLine();
        ticketDao.persist(ticket);
        busLineDao.persist(busLine);
        Ticket expResult = ticket;
        expResult.setBusLine(busLine);
        Long expId = ticket.getId();
        
        Ticket result = ticketService.updateTicketBusLine(ticket, busLine);
        assertEquals(expResult, result);
        assertEquals(expResult.getBusLine(), result.getBusLine());
        result = ticketDao.find(expId);
        assertEquals(expResult, result);
    }

    
    /**
     * Test of updateTicketStartingDestination method, of class TicketService.
     * Testing with:    null
     * Should NOT change starting Destination
     */
    @Test
    public void testUpdateTicketStartingDestination_nullDestination_doesntChangeStartingDestination() {
        System.out.println("updateTicketStartingDestination");
        Ticket ticket = Generator.generateTicket();
        ticketDao.persist(ticket);
        Destination startingDestination = null;
        Destination expResult = ticket.getStartingDestination();
        
        ticketService.updateTicketStartingDestination(ticket, startingDestination);
        Destination result = ticket.getStartingDestination();
        assertEquals(expResult, result);
    }

    
    /**
     * Test of updateTicketStartingDestination method, of class TicketService.
     * Testing with:    generated Destination
     * Should change starting Destination
     */
    @Test
    public void testUpdateTicketStartingDestination_validDestination_changesStartingDestination() {
        System.out.println("updateTicketStartingDestination");
        Ticket ticket = Generator.generateTicket();
        ticketDao.persist(ticket);
        BusLine busLine = Generator.generateBusLine();
        Date nextHour = new Date();
        nextHour.setTime(nextHour.getTime() + 3600000);
        busLine.setDateTime(nextHour);
        List<DestinationKilometerMinutes> lineDestinations = busLine.getDestinations();
        lineDestinations.add(1, new DestinationKilometerMinutes(busLine, ticket.getFinalDestination(), 10, 6));
        busLine.setDestinations(lineDestinations);
        busLineDao.persist(busLine);
        Destination startingDestination = busLine.getStartingDestination();
        Destination expResult = startingDestination;
        
        ticketService.updateTicketStartingDestination(ticket, startingDestination);
        Destination result = ticket.getStartingDestination();
        assertEquals(expResult, result);
    }

    
    /**
     * Test of updateTicketFinalDestination method, of class TicketService.
     * Testing with:    null
     * Should NOT change final Destination
     */
    @Test
    public void testUpdateTicketFinalDestination_nullDestination_doesntChangeFinalDestination() {
        System.out.println("updateTicketFinalDestination");
        Ticket ticket = Generator.generateTicket();
        ticketDao.persist(ticket);
        Destination finalDestination = null;
        Destination expResult = ticket.getFinalDestination();
        
        ticketService.updateTicketFinalDestination(ticket, finalDestination);
        Destination result = ticket.getFinalDestination();
        assertEquals(expResult, result);
    }

    
    /**
     * Test of updateTicketFinalDestination method, of class TicketService.
     * Testing with:    generated Destination
     * Should change final Destination
     */
    @Test
    public void testUpdateTicketFinalDestination_validDestination_changesFinalDestination() {
        System.out.println("updateTicketFinalDestination");
        Ticket ticket = Generator.generateTicket();
        ticketDao.persist(ticket);
        BusLine busLine = Generator.generateBusLine();
        Date nextHour = new Date();
        nextHour.setTime(nextHour.getTime() + 3600000);
        busLine.setDateTime(nextHour);
        List<DestinationKilometerMinutes> lineDestinations = busLine.getDestinations();
        lineDestinations.add(lineDestinations.size() - 1, new DestinationKilometerMinutes(busLine, ticket.getStartingDestination(), 10, 6));
        busLine.setDestinations(lineDestinations);
        busLineDao.persist(busLine);
        Destination finalDestination = busLine.getFinalDestination();
        Destination expResult = finalDestination;
        
        ticketService.updateTicketFinalDestination(ticket, finalDestination);
        Destination result = ticket.getFinalDestination();
        assertEquals(expResult, result);
    }

   
    /**
     * Test of removeTicket method, of class TicketService.
     * Testing with:    null
     * Should remove nothing
     */
    @Test
    public void testRemoveTicket_nullTicket_doesNotRemoveAnything() {
        System.out.println("removeTicket");
        Ticket ticket = null;
        List<Ticket> expList = ticketDao.findAll();
        
        ticketService.removeTicket(ticket);
        List<Ticket> resultList = ticketDao.findAll();
        assertEquals(expList, resultList);
    }

   
    /**
     * Test of removeTicket method, of class TicketService.
     * Testing with:    generated Ticket
     * Should remove generated Ticket
     */
    @Test
    public void testRemoveTicket_validTicket_removesGivenTicket() {
        System.out.println("removeTicket");
        Ticket ticket = Generator.generateTicket();
        ticketDao.persist(ticket);
        List<Ticket> expList = ticketDao.findAll();
        expList.remove(ticket);
        
        ticketService.removeTicket(ticket);
        List<Ticket> resultList = ticketDao.findAll();
        assertEquals(expList, resultList);
    }
}
