package cz.cvut.fel.ear.bussystem.service;

import cz.cvut.fel.ear.bussystem.dao.BusLineDao;
import cz.cvut.fel.ear.bussystem.dao.DriverDao;
import cz.cvut.fel.ear.bussystem.environment.*;
import cz.cvut.fel.ear.bussystem.model.BusLine;
import cz.cvut.fel.ear.bussystem.model.Driver;
import cz.cvut.fel.ear.bussystem.model.User;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@TestPropertySource(locations = "classpath:application-test.properties")
public class DriverServiceTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    @Autowired
    private DriverDao driverDao;
    @Autowired
    private BusLineDao busLineDao;
    @Autowired
    private DriverService driverService;
    //private List<Driver> cleanUpDrivers;
    //private List<BusLine> cleanUpLines;
    
    public DriverServiceTest() {
    }
    
    /*@Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        cleanUpDrivers = new ArrayList<>();
        cleanUpLines = new ArrayList<>();
    }
    
    @After
    public void tearDown() {
        if (!cleanUpDrivers.isEmpty()) {
            for (Driver driver : cleanUpDrivers) {
                driverDao.remove(driver);
            }
        }
        if (!cleanUpLines.isEmpty()) {
            for (BusLine line : cleanUpLines) {
                busLineDao.remove(line);
            }
        }
    }*/

    /**
     * Test of find method, of class DriverService.
     * Testing with:    null
     * Should return:   null
     */
    @Test
    public void testFind_nullId_returnsNull() {
        System.out.println("find");
        Integer id = null;
        Driver expResult = null;
        
        Driver result = driverService.find(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of find method, of class DriverService.
     * Testing with:    negative random int
     * Should return:   null
     */
    @Test
    public void testFind_negativeId_returnsNull() {
        System.out.println("find");
        int id = (int) (- Math.random() * 100);
        Driver expResult = null;
        
        Driver result = driverService.find(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of addDriver method, of class DriverService.
     * Testing with:    null
     * Should return:   null
     */
    @Test
    public void testAddDriver_nullDriver_doesNotAddAnything() {
        System.out.println("addDriver");
        Driver driver = null;
        Driver expResult = null;
        List<Driver> expList = driverDao.findAll();
        
        Driver result = driverService.addDriver(driver);
        assertEquals(expResult, result);
        
        List<Driver> resultList = driverDao.findAll();
        assertEquals(expList, resultList);
    }

    /**
     * Test of addDriver method, of class DriverService.
     * Testing with:    new Driver with minimum info
     * Should return:   the created Driver
     */
    @Test
    public void testAddDriver_validDriver_addsBusWithHighestId() {
        System.out.println("addDriver");
        Driver driver = Generator.generateDriver();
        Driver expResult = driver;
        List<Driver> expList = driverDao.findAll();
        expList.add(driver);
        
        Driver result = driverService.addDriver(driver);
        Integer expId = driver.getId();
        assertEquals(expResult, result);
        result = driverDao.find(expId);
        assertEquals(expResult, result);
        
        List<Driver> resultList = driverDao.findAll();
        assertEquals(expList, resultList);
        
        //cleanUpDrivers.add(driver);
    }

    /**
     * Test of olderDriver method, of class DriverService.
     * Testing with:    new Driver with minimum info
     * Should return:   the created Driver oldered by 1 year
     */
    @Test
    public void testOlderDriver_validDriver_updatesDriver() {
        System.out.println("olderDriver");
        Driver driver = Generator.generateDriver();
        driverDao.persist(driver);
        Integer expId = driver.getId();
        Driver expResult = driver;
        expResult.setAge(51);
        
        driverService.olderDriver(driver);
        Driver result = driverDao.find(expId);
        assertEquals(expResult, result);
        
        //cleanUpDrivers.add(driver);
    }

    /**
     * Test of addMinutesDrivenInRow method, of class DriverService.
     * Testing with:    new Driver with minimum info, -10
     * Should return:   the created Driver unchanged
     */
    @Test
    public void testAddMinutesDrivenInRow_negativeMinutes_doesNotChangeDriver() {
        System.out.println("addMinutesDrivenInRow");
        Driver driver = Generator.generateDriver();
        driverDao.persist(driver);
        Integer expId = driver.getId();
        Driver expResult = driver;
        
        driverService.addMinutesDrivenInRow(driver, -10);
        Driver result = driverDao.find(expId);
        assertEquals(expResult, result);
        
        //cleanUpDrivers.add(driver);
    }

    /**
     * Test of addMinutesDrivenInRow method, of class DriverService.
     * Testing with:    new Driver with minimum info, 10
     * Should return:   the created Driver with raised minutesDrivenInRow
     */
    @Test
    public void testAddMinutesDrivenInRow_positiveMinutes_changesDriver() {
        System.out.println("addMinutesDrivenInRow");
        Driver driver = Generator.generateDriver();
        driverDao.persist(driver);
        Integer expId = driver.getId();
        Driver expResult = driver;
        expResult.setMinutesDrivenInRow(expResult.getMinutesDrivenInRow() + 10);
        
        driverService.addMinutesDrivenInRow(driver, 10);
        Driver result = driverDao.find(expId);
        assertEquals(expResult, result);
        
        //cleanUpDrivers.add(driver);
    }

    /**
     * Test of resetMinutesDrivenInRow method, of class DriverService.
     * Testing with:    null
     * Should return:   unchanged list of all Drivers
     */
    @Test
    public void testResetMinutesDrivenInRow_nullDriver_doesNothing() {
        System.out.println("resetMinutesDrivenInRow");
        Driver driver = null;
        List<Driver> expList = driverDao.findAll();
        
        driverService.resetMinutesDrivenInRow(driver);
        List<Driver> resultList = driverDao.findAll();
        assertEquals(expList, resultList);
    }
    
    /**
     * Test of resetMinutesDrivenInRow method, of class DriverService.
     * Testing with:    new Driver with minimum info
     * Should return:   the created Driver with minutesDrivenInRow set to 0
     */
    @Test
    public void testResetMinutesDrivenInRow_validDriver_resetsDriversMinutes() {
        System.out.println("resetMinutesDrivenInRow");
        Driver driver = Generator.generateDriver();
        driverDao.persist(driver);
        Integer expId = driver.getId();
        Driver expResult = driver;
        expResult.setMinutesDrivenInRow(0);
        
        driverService.resetMinutesDrivenInRow(driver);
        Driver result = driverDao.find(expId);
        assertEquals(expResult, result);
        
        //cleanUpDrivers.add(driver);
    }

    /**
     * Test of updateDriverLineToDriver method, of class DriverService.
     * Testing with:    new Driver with minimum info, null
     * Should return:   the created Driver
     */
    @Test
    public void testUpdateBusLineToDriver_nullBusLine_doesNothing() {
        System.out.println("updateDriverLineToDriver");
        Driver driver = Generator.generateDriver();
        BusLine line = null;
        driverDao.persist(driver);
        Integer expId = driver.getId();
        Driver expResult = driver;
        
        driverService.updateBusLineToDriver(driver, line);
        Driver result = driverDao.find(expId);
        assertEquals(expResult, result);
        
        //cleanUpDrivers.add(driver);
    }
    
    /**
     * Test of updateDriverLineToDriver method, of class DriverService.
     * Testing with:    new Driver with minimum info, new BusLine with minimum info
     * Should return:   the created Driver with created BusLine
     */
    @Test
    public void testUpdateBusLineToDriver_validBusLine_signsBusLineToDriver() {
        System.out.println("updateDriverLineToDriver");
        Driver driver = Generator.generateDriver();
        BusLine line = Generator.generateBusLine();
        List<BusLine> lines = new ArrayList<>();
        lines.add(line);
        driverDao.persist(driver);
        busLineDao.persist(line);
        Integer expId = driver.getId();
        Driver expResult = driver;
        expResult.setLines(lines);
        
        driverService.updateBusLineToDriver(driver, line);
        Driver result = driverDao.find(expId);
        assertEquals(expResult, result);
        
        //cleanUpDrivers.add(driver);
        //cleanUpLines.add(line);
    }

    /**
     * Test of updateEmail method, of class DriverService.
     * Testing with:    new Driver with minimum info, "test@update.email"
     * Should return:   the created Driver with changed email
     */
    @Test
    public void testUpdateEmail_validEmail_changesDriversEmail() {
        System.out.println("updateEmail");
        Driver driver = Generator.generateDriver();
        String email = driver.getEmail();
        driverDao.persist(driver);
        Integer expId = driver.getId();
        Driver expResult = driver;
        expResult.setEmail(email);
        
        driverService.updateEmail(driver, email);
        Driver result = driverDao.find(expId);
        assertEquals(expResult, result);
        
        //cleanUpDrivers.add(driver);
    }

    /**
     * Test of removeDriver method, of class DriverService.
     * Testing with:    null
     * Should return:   null
     */
    @Test
    public void testRemoveDriver_nullDriver_removesNothing() {
        System.out.println("removeDriver");
        Driver driver = null;
        List<Driver> expList = driverDao.findAll();
        
        driverService.removeDriver(driver);
        List<Driver> resultList = driverDao.findAll();
        assertEquals(expList, resultList);
    }

    /**
     * Test of removeDriver method, of class DriverService.
     * Testing with:    new Driver with minimum info
     * Should return:   unchanged list of all Drivers
     */
    @Test
    public void testRemoveDriver_validDriver_removesGivenDriver() {
        System.out.println("removeDriver");
        Driver driver = Generator.generateDriver();
        driverDao.persist(driver);
        //cleanUpDrivers.add(driver);
        List<Driver> expList = driverDao.findAll();
        expList.remove(driver);
        
        driverService.removeDriver(driver);
        List<Driver> resultList = driverDao.findAll();
        assertEquals(expList, resultList);
        
        //cleanUpDrivers.remove(driver);
    }

    /**
     * Test of exists method, of class DriverService.
     * Testing with:    null
     * Should return:   false
     */
    @Test
    public void testExists_nullUsername_returnsFalse() {
        System.out.println("exists");
        
        assertFalse(driverService.exists(null));
    }

    /**
     * Test of exists method, of class DriverService.
     * Testing with:    new Driver's username
     * Should return:   true
     */
    @Test
    public void testExists_validUsername_returnsTrue() {
        System.out.println("exists");
        Driver driver = Generator.generateDriver();
        String username = driver.getUsername();
        driverDao.persist(driver);
        //cleanUpDrivers.add(driver);
        
        assertTrue(driverService.exists(username));
    }

    /**
     * Test of validatePassword method, of class DriverService.
     * Testing with:    new Driver's username, "nonsense"
     * Should return:   null
     */
    @Test
    public void testValidatePassword_wrongPassword_returnsNull() {
        System.out.println("validatePassword");
        Driver driver = Generator.generateDriver();
        String username = driver.getUsername();
        driverDao.persist(driver);
        //cleanUpDrivers.add(driver);
        User ExpResult = null;
        
        User result = driverService.validatePassword(username, "nonsense");
        assertEquals(ExpResult, result);
    }

    /**
     * Test of validatePassword method, of class DriverService.
     * Testing with:    new Driver's username, new Driver's password
     * Should return:   the created Driver
     */
    @Test
    public void testValidatePassword_validPassword_returnsDriverl() {
        System.out.println("validatePassword");
        Driver driver = Generator.generateDriver();
        String username = driver.getUsername();
        String password = driver.getPassword();
        driverDao.persist(driver);
        //cleanUpDrivers.add(driver);
        User ExpResult = driver;
        
        User result = driverService.validatePassword(username, password);
        assertEquals(ExpResult, result);
    }
    
}
