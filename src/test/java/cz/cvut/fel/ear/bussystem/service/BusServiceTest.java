package cz.cvut.fel.ear.bussystem.service;

import cz.cvut.fel.ear.bussystem.dao.BusDao;
import cz.cvut.fel.ear.bussystem.environment.*;
import cz.cvut.fel.ear.bussystem.model.Bus;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@TestPropertySource(locations = "classpath:application-test.properties")
public class BusServiceTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    @Autowired
    private BusDao busDao;
    @Autowired
    private BusService busService;
    //private List<Bus> cleanUp;
    
    public BusServiceTest() {
    }

    /*@Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        cleanUp = new ArrayList<>();
    }
    
    @After
    public void tearDown() {
        if (!cleanUp.isEmpty()) {
            for (Bus bus : cleanUp) {
                busDao.remove(bus);
            }
        }
    }*/

    /**
     * Test of find method, of class BusService.
     * Testing with:    null
     * Should return:   null
     */
    @Test
    public void testFind_nullId_returnsNull() {
        System.out.println("find");
        Integer id = null;
        Bus expResult = null;
        
        Bus result = busService.find(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of find method, of class BusService.
     * Testing with:    negative random int
     * Should return:   null
     */
    @Test
    public void testFind_negativeId_returnsNull() {
        System.out.println("find");
        int id = (int) (- Math.random() * 100);
        Bus expResult = null;
        
        Bus result = busService.find(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of find method, of class BusService.
     * Testing with:    id of created Bus
     * Should return:   created Bus
     */
    @Test
    public void testFind_validId_returnsBus() {
        System.out.println("find");
        Bus bus = Generator.generateBus();
        busDao.persist(bus);
        //cleanUp.add(bus);
        int id = bus.getId();
        Bus expResult = bus;
        
        Bus result = busService.find(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of addBus method, of class BusService.
     * Testing with:    null
     * Should return:   null
     */
    @Test
    public void testAddBus_nullBus_doesNotAddAnything() {
        System.out.println("addBus");
        Bus bus = null;
        Bus expResult = null;
        List<Bus> expList = busDao.findAll();
        
        Bus result = busService.addBus(bus);
        assertEquals(expResult, result);
        
        List<Bus> resultList = busDao.findAll();
        assertEquals(expList, resultList);
    }

    /**
     * Test of addBus method, of class BusService.
     * Testing with:    new Bus with minimum info
     * Should return:   created Bus
     */
    @Test
    public void testAddBus_validBus_addsBusWithHighestId() {
        System.out.println("addBus");
        Bus bus = Generator.generateBus();
        Bus expResult = bus;
        List<Bus> expList = busDao.findAll();
        expList.add(bus);
        
        Bus result = busService.addBus(bus);
        Integer id = bus.getId();
        assertEquals(expResult, result);
        result = busDao.find(id);
        assertEquals(expResult, result);
        
        List<Bus> resultList = busDao.findAll();
        assertEquals(expList, resultList);
        
        //cleanUp.add(bus);
    }

    /**
     * Test of updateBus method, of class BusService.
     * Testing with:    new Bus with minimum info changed
     * Should return:   changed Bus
     */
    @Test
    public void testUpdateBus_validBus_updatesBus() {
        System.out.println("updateBus");
        Bus bus = Generator.generateBus();
        busDao.persist(bus);
        //cleanUp.add(bus);
        Integer expId = bus.getId();
        bus.setDescription("Updated bus.");
        bus.setDrivenKilometers(1000);
        Bus expResult = bus;
        
        Bus result = busService.updateBus(bus);
        assertEquals(expResult, result);
        result = busDao.find(expId);
        assertEquals(expResult, result);
        
        //cleanUp.add(bus);
    }

    /**
     * Test of addDrivenKilometers method, of class BusService.
     * Testing with:    new Bus with minimum info, 500
     * Should return:   created Bus with raised drivenKilometer
     */
    @Test
    public void testAddDrivenKilometers_validBus_updatesBusKilometers() {
        System.out.println("addDrivenKilometers");
        Bus bus = Generator.generateBus();
        busDao.persist(bus);
        //cleanUp.add(bus);
        int kilometers = 500;
        Integer expId = bus.getId();
        Bus expResult = bus;
        expResult.setDrivenKilometers(kilometers + bus.getDrivenKilometers());
        
        busService.addDrivenKilometers(bus, kilometers);
        Bus result = busDao.find(expId);
        assertEquals(expResult, result);
        assertEquals(result.getDrivenKilometers(), expResult.getDrivenKilometers());
        
        //cleanUp.add(bus);
    }

    /**
     * Test of removeBus method, of class BusService.
     * Testing with:    null
     * Should return:   unchanged list of all Buses
     */
    @Test
    public void testRemoveBus_nullBus_doesNotRemoveAnything() {
        System.out.println("removeBus");
        Bus bus = null;
        List<Bus> expList = busDao.findAll();
        
        busService.removeBus(bus);
        List<Bus> resultList = busDao.findAll();
        assertEquals(expList, resultList);
    }
    
    /**
     * Test of removeBus method, of class BusService.
     * Testing with:    new Bus with minimum info
     * Should return:   list of all Buses reduced by the created bus
     */
    @Test
    public void testRemoveBus_validBus_removesGivenBus() {
        System.out.println("removeBus");
        Bus bus = Generator.generateBus();
        busDao.persist(bus);
        //cleanUp.add(bus);
        List<Bus> expList = busDao.findAll();
        expList.remove(bus);
        
        busService.removeBus(bus);
        List<Bus> resultList = busDao.findAll();
        assertEquals(expList, resultList);
        
        //cleanUp.remove(bus);
    }
}
