package cz.cvut.fel.ear.bussystem.dao;

import cz.cvut.fel.ear.bussystem.BusSystemApplication;
import cz.cvut.fel.ear.bussystem.environment.*;
import cz.cvut.fel.ear.bussystem.model.Destination;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

// Spring runner for JUnit
@RunWith(SpringRunner.class)
// DataJpaTest does not load all the application beans, it starts only persistence-related stuff
@DataJpaTest
// Exclude SystemInitializer from the startup, we don't want the admin account here
@ComponentScan(basePackageClasses = BusSystemApplication.class)
public class DestinationDaoTest {   
    @PersistenceContext
    protected EntityManager em;
    @Autowired
    private DestinationDao destinationDao;
    //private List<Destination> cleanUp;
    
    public DestinationDaoTest() {
    }
    
    /*@Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        cleanUp = new ArrayList<>();
    }
    
    @After
    public void tearDown() {
        if (!cleanUp.isEmpty()) {
            for (Destination destination : cleanUp) {
                em.remove(destination);
            }
        }
    }*/

    /**
     * Test of findAll method, of class DestinationDao.
     * Should return:   the same as EntityManager for "SELECT d FROM Destination WHERE NOT d.removed"
     */
    @Test
    public void testFindAll_noInput_returnsAllDestinationsInDB() {
        System.out.println("findAll");
        List<Destination> expResult = em.createQuery("SELECT d FROM Destination d", Destination.class).getResultList();
        
        List<Destination> result = destinationDao.findAll();
        assertEquals(expResult, result);
    }

    /**
     * Test of find method, of class DestinationDao.
     * Testing with:    null
     * Should return:   null
     */
    @Test
    public void testFind_nullId_returnsNull() {
        System.out.println("find");
        Integer id = null;
        Destination expResult = null;
        
        Destination result = destinationDao.find(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of find method, of class DestinationDao.
     * Testing with:    new Destination with minimum info
     * Should return:   created Destination
     */
    @Test
    public void testFind_validId_returnsDestination() {
        System.out.println("find");
        Destination destination = Generator.generateDestination();
        em.persist(destination);
        //cleanUp.add(destination);
        int id = destination.getId();
        Destination expResult = destination;
        
        Destination result = destinationDao.find(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of findConnectedDestinations method, of class DestinationDao.
     * Testing with:    null
     * Should return:   null
     */
    @Test
    public void testFindConnectedDestinations_nullId_returnsNull() {
        System.out.println("findConnectedDestinations");
        Integer id = null;
        List<Destination> expResult = null;
        
        List<Destination> result = destinationDao.findConnectedDestinations(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of findConnectedDestinations method, of class DestinationDao.
     * Testing with:    new Destination without any connected Destinations
     * Should return:   null
     */
    @Test
    public void testFindConnectedDestinations_validIdOfDisconnectedDestination_returnsListOfDestinations() {
        System.out.println("findConnectedDestinations");
        Destination destination = Generator.generateDestination();
        em.persist(destination);
        //cleanUp.add(destination);
        int id = destination.getId();
        List<Destination> expResult = destination.getConnectedDestinations();
        
        List<Destination> result = destinationDao.findConnectedDestinations(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of findConnectedDestinations method, of class DestinationDao.
     * Testing with:    new Destination with prepared connected Destinations
     * Should return:   connected Destinations
     */
    @Test
    public void testFindConnectedDestinations_validIdOfConnectedDestination_returnsConnectedDestinations() {
        System.out.println("findConnectedDestinations");
        Destination destination = Generator.generateDestination();
        // filling connected Destinations
        List<Destination> testedDestination = new ArrayList<>();
        testedDestination.add(destination);
        List<Destination> connectedDestinations = new ArrayList<>();
        for (short i = 0; i < 5; i++) {
            Destination connected = Generator.generateDestination();
            connected.setConnectedDestinations(testedDestination);
            em.persist(connected);
            //cleanUp.add(connected);
        }
        destination.setConnectedDestinations(connectedDestinations);
        em.persist(destination);
        //cleanUp.add(destination);
        int id = destination.getId();
        List<Destination> expResult = connectedDestinations;
        
        List<Destination> result = destinationDao.findConnectedDestinations(id);
        assertEquals(expResult, result);
    }
}
