package cz.cvut.fel.ear.bussystem.environment;

import cz.cvut.fel.ear.bussystem.BusSystemApplication;
import cz.cvut.fel.ear.bussystem.model.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

// Spring runner for JUnit
//@RunWith(SpringRunner.class)
// DataJpaTest does not load all the application beans, it starts only persistence-related stuff
@DataJpaTest
// Exclude SystemInitializer from the startup, we don't want the admin account here
@ComponentScan(basePackageClasses = BusSystemApplication.class)
public class Generator {
    /*@Autowired
    protected static AdministratorDao adminDao;
    @Autowired
    protected static CustomerDao customerDao;
    @Autowired
    protected static DriverDao driverDao;
    @Autowired
    protected static BusDao busDao;
    @Autowired
    protected static DestinationDao destinationDao;
    @Autowired
    protected static BusLineDao busLineDao;
    @Autowired
    protected static TicketDao ticketDao;*/
    
    private static int adminId = 1;
    private static int customerId = 1;
    private static int driverId = 1;
    private static int busId = 1;
    private static int destinationId = 1;
    private static int busLineId = 1;
    private static long ticketId = 1;

    private static final Random RAND = new Random();

    public static int randomInt() {
        return RAND.nextInt();
    }

    public static int randomInt(int min, int max) {
        if (min > max) {
            int temp = min;
            min = max;
            max = temp;
        }
        return min + RAND.nextInt(max - min);
    }

    public static boolean randomBoolean() {
        return RAND.nextBoolean();
    }

    
    public static Administrator generateAdmin() {
        //while (adminDao.exists(adminId)) {
            adminId++;
        //}
        //adminId = randomInt(0, 5000);
        Administrator ret = new Administrator();
        //ret.setId(adminId++);
        ret.setUsername("admin" + adminId);
        ret.setPassword(ret.getUsername() + "pass");
        ret.setEmail(ret.getUsername() + "@admin.test");
        ret.setLevel(1);
        return ret;
    }
    
    public static Customer generateCustomer() {
        //while (customerDao.exists(customerId)) {
            customerId++;
        //}
        //customerId = randomInt(0, 5000);
        Customer ret = new Customer();
        //ret.setId(customerId++);
        ret.setUsername("customer" + customerId);
        ret.setPassword(ret.getUsername() + "pass");
        ret.setEmail(ret.getUsername() + "@customer.test");
        ret.setName("customer " + customerId);
        return ret;
    }
    
    public static Driver generateDriver() {
        //while (driverDao.exists(driverId)) {
            driverId++;
        //}
        //driverId = randomInt(0, 5000);
        Driver ret = new Driver();
        //ret.setId(driverId++);
        ret.setUsername("driver" + driverId);
        ret.setPassword(ret.getUsername() + "pass");
        ret.setEmail(ret.getUsername() + "@driver.test");
        ret.setAge(randomInt(20, 80));
        ret.setLines(new ArrayList<>());
        return ret;
    }
    
    public static Bus generateBus() {
        //while (busDao.exists(busId)) {
            busId++;
        //}
        //busId = randomInt(0, 5000);
        Bus ret = new Bus();
        //ret.setId(busId++);
        ret.setDescription("Test bus (" + busId + ") with random specifications.");
        ret.setDrivenKilometers(randomInt());
        ret.setSeats(4 * randomInt(4, 12));
        ret.setYearOfManufacture(randomInt(1980, 2019));
        switch (randomInt(0, 5)) {
            case 0:
                ret.setLuggageSpace(Luggage.NONE);
                break;
            case 1:
                ret.setLuggageSpace(Luggage.HAND_LUGGAGE);
                break;
            case 2:
                ret.setLuggageSpace(Luggage.LESS_THAN_8M_SQUARE);
                break;
            default:
                ret.setLuggageSpace(Luggage.MORE_THAN_8M_SQUARE);
                break;
        }
        return ret;
    }
    
    public static Destination generateDestination() {
        //while (destinationDao.exists(destinationId)) {
            destinationId++;
        //}
        //destinationId = randomInt(0, 5000);
        Destination ret = new Destination();
        //ret.setId(destinationId++);
        ret.setCountry(Country.HUNGARY);
        ret.setName("TestCity" + destinationId);
        return ret;
    }
    
    public static BusLine generateBusLine() {
        //while (busLineDao.exists(busLineId)) {
            busLineId++;
        //}
        //busLineId = randomInt(0, 5000);
        BusLine ret = new BusLine();
        //ret.setId(busLineId++);
        ret.setBus(generateBus());
        ret.setDateTime(new Date(2020, randomInt(1, 12), randomInt(1, 28), randomInt(6, 22), 6 * randomInt(0, 10)));
        List<DestinationKilometerMinutes> destinations = new ArrayList<>();
        for (int i = 0; i < randomInt(2, 8); i++) {
            int kilometers = randomInt(10, 1000);
            destinations.add(new DestinationKilometerMinutes(ret, generateDestination(), kilometers, kilometers / 2));
        }
        ret.setDestinations(destinations);
        ret.setDriver(generateDriver());
        List<BusLine> driverLines = ret.getDriver().getLines();
        driverLines.add(ret);
        ret.getDriver().setLines(driverLines);
        switch (randomInt(0, 5)) {
            case 0:
                ret.setRegularity(Regularity.DAILY);
                break;
            case 1:
                ret.setRegularity(Regularity.WORKDAY);
                break;
            case 2:
                ret.setRegularity(Regularity.WEEKLY);
                break;
            default:
                ret.setRegularity(Regularity.MONTHLY);
                break;
        }
        return ret;
    }
    
    public static Ticket generateTicket() {
        //while (ticketDao.exists(ticketId)) {
            ticketId++;
        //}
        //ticketId = randomInt(0, 5000);
        Ticket ret = new Ticket();
        //ret.setId(ticketId++);
        ret.setBusLine(generateBusLine());
        ret.setCustomer(generateCustomer());
        ret.setDateTime(new Date());
        ret.setStartingDestination(ret.getBusLine().getStartingDestination());
        ret.setFinalDestination(ret.getBusLine().getFinalDestination());
        return ret;
    }
}
