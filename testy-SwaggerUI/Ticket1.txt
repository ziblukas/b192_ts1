{
  "arrivalTime": "2020-01-05T12:10:42.141Z",
  "busLine": {
  "bus": {
    "description": "Testing from Karlstejn",
    "drivenKilometers": 0,
    "luggageSpace": "NONE",
    "seats": 0,
    "yearOfManufacture": 2020
  },
  "dateTime": "2020-01-05T12:10:42.138Z",
  "destinations": [
    {
      "destination": {
        "connectedDestinations": [
          {
            "destination": {
              "country": "CZECH",
              "name": "Karlstejn2"
            }
          }
        ],
        "country": "CZECH",
        "name": "Karlstejn1"
      },
      "kilometersFromPrevious": 0,
      "minutesFromPrevious": 0
    },
    {
      "destination": {
        "connectedDestinations": [
          {
            "destination": {
              "country": "CZECH",
              "name": "Karlstejn1"
            }
          }
        ],
        "country": "CZECH",
        "name": "Karlstejn2"
      },
      "kilometersFromPrevious": 10,
      "minutesFromPrevious": 6
    }
  ],
  "driver": {
    "age": 22,
    "email": "test@karlstejn.cz",
    "lines": [
      null
    ],
    "minutesDrivenInRow": 0,
    "password": "karlstejnak1pass",
    "role": "string",
    "username": "karlstejnak1"
  },
  "finalDestination": {
      "destination": {
        "connectedDestinations": [
          {
            "destination": {
              "country": "CZECH",
              "name": "Karlstejn1"
            }
          }
        ],
        "country": "CZECH",
        "name": "Karlstejn2"
      },
      "kilometersFromPrevious": 10,
      "minutesFromPrevious": 6
    },
  "regularity": "DAILY",
  "startingDestination": {
      "destination": {
        "connectedDestinations": [
          {
            "destination": {
              "country": "CZECH",
              "name": "Karlstejn2"
            }
          }
        ],
        "country": "CZECH",
        "name": "Karlstejn1"
      },
      "kilometersFromPrevious": 0,
      "minutesFromPrevious": 0
    }
},
  "customer": {
    "email": "test@customer.cz",
    "name": "customer1 karlstejnak",
    "password": "customer1pass",
    "role": "string",
    "tickets": [
      null
    ],
    "username": "customer1"
  },
  "dateTime": "2020-01-05T12:10:42.141Z",
  "departureTime": "2020-01-05T12:10:42.141Z",
  "finalDestination": {
      "destination": {
        "connectedDestinations": [
          {
            "destination": {
              "country": "CZECH",
              "name": "Karlstejn1"
            }
          }
        ],
        "country": "CZECH",
        "name": "Karlstejn2"
      }
    },
  "length": 10,
  "startingDestination": {
      "destination": {
        "connectedDestinations": [
          {
            "destination": {
              "country": "CZECH",
              "name": "Karlstejn2"
            }
          }
        ],
        "country": "CZECH",
        "name": "Karlstejn1"
      }
    }
}